class CreateBusinesses < ActiveRecord::Migration[5.1]
  def change
    create_table :businesses do |t|
    	t.string :name
      t.string :tin
      t.boolean :vat
      t.string :address
      t.string :proprietor
      t.string :mobile
      t.string :address
      t.string :email
      t.boolean :include_wholesale, default: false
      
      t.timestamps
    end
  end
end
