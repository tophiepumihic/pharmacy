class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :last_name
      t.string :first_name
      t.string :middle_name
      t.string :full_name
      t.integer :member_type
      t.string :mobile
      t.string :type
      t.boolean :has_credit, default: false

      t.timestamps
    end
  end
end
