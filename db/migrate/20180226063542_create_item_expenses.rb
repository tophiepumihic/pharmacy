class CreateItemExpenses < ActiveRecord::Migration[5.1]
  def change
    create_table :item_expenses do |t|
      t.belongs_to :stock, foreign_key: true
      t.belongs_to :product, foreign_key: true
      t.integer :expense_type
      t.decimal :quantity
      t.integer :employee_id, foreign_key: true, index: true

      t.timestamps
    end
  end
end
