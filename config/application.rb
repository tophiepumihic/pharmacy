require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Pharmacy
  class Application < Rails::Application
    config.time_zone = 'Asia/Manila'
    config.active_record.default_timezone = :utc
    config.active_record.belongs_to_required_by_default = false
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
  end
end
