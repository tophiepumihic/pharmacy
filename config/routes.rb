Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => "users", sessions: "users/sessions" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "store/index"
  get 'products/index'
  get "account/index"


  root :to => "store#index", :constraints => lambda { |request| request.env['warden'].user.nil? }, as: :unauthenticated_root
  root :to => 'store#index', :constraints => lambda { |request| request.env['warden'].user.role == ('admin' || 'proprietor' || 'developer') if request.env['warden'].user }, as: :admin_root
  root :to => 'store#index', :constraints => lambda { |request| request.env['warden'].user.role == 'cashier' if request.env['warden'].user }, as: :cashier_root
  root :to => 'store#index', :constraints => lambda { |request| request.env['warden'].user.role == 'retail_cashier' if request.env['warden'].user }, as: :retail_cashier_root
  root :to => 'store#index', :constraints => lambda { |request| request.env['warden'].user.role == 'wholesale_cashier' if request.env['warden'].user }, as: :wholesale_cashier_root

  resources :products do
    get :autocomplete_product_name_and_description, on: :collection
    match "/scope_to_category" => "products#scope_to_category", as: :scope_to_category, via: [:get], on: :collection
    resources :stocks, module: :products do
      match "/delete" => "stocks#delete", as: :delete, via: [:get, :post]
      match "/add_to_expenses" => "stocks#add_to_expenses", as: :add_to_expenses, via: [:get, :post]
      match "/remove_from_expenses" => "stocks#remove_from_expenses", as: :remove_from_expenses, via: [:get, :post]
      resources :item_expenses, module: :stocks do
        match "/remove_from_expenses" => "item_expenses#remove_from_expenses", as: :remove_from_expenses, via: [:get, :post]
      end
    end
    resources :barcodes, module: :products
    match "/cash_sales" => "products#cash_sales", as: :cash_sales, via: [:get], on: :member
    match "/credit_sales" => "products#credit_sales", as: :credit_sales, via: [:get], on: :member
    match "/stock_histories" => "products#stock_histories", as: :stock_histories, via: [:get], on: :member
    match "/deleted" => "products#deleted", as: :deleted, via: [:get], on: :member
    match "/barcode" => "products#barcode", as: :barcode, via: [:get], on: :member
    match "/reports" => "products#reports", as: :reports, via: [:get], on: :member
    match "/scope_to_date_cash_sales" => "products#scope_to_date_cash_sales",  via: [:get], on: :member
    match "/scope_to_date_credit_sales" => "products#scope_to_date_credit_sales",  via: [:get], on: :member
    match "/scope_to_date_sales" => "products#scope_to_date_sales",  via: [:get], on: :member
    collection { post :import}
    match "/download" => "products#download", as: :download, via: [:get], on: :collection
    match "/export" => "products#export", as: :export, via: [:get], on: :collection
  end
  namespace :products do
    resources :imports, only: [:create]
  end
  resources :barcodes do
    collection { post :import}
  end

  resources :credit_collections do
    match "/scope_to_date" => "credit_collections#scope_to_date",  via: [:get], on: :collection
  end

  resources :carts
  resources :line_items do
    match "/return" => "line_items#return", as: :return, via: [:get, :post]
  end
  resources :orders do
    match "/guest" => "orders#guest",  via: [:post], on: :member
    match "/print" => "orders#print",  via: [:get], on: :member
    match "/print_invoice" => "orders#print_invoice",  via: [:get], on: :member
    match "/print_official_receipt" => "orders#print_official_receipt",  via: [:get], on: :member
    match "/scope_to_date" => "orders#scope_to_date",  via: [:get], on: :collection
    match "/monthly_summary_report" => "orders#monthly_summary_report",  via: [:get], on: :collection
    match "/retail_and_wholesale" => "orders#retail_and_wholesale", as: :retail_and_wholesale, via: [:get], on: :collection
    match "/retail" => "orders#retail", as: :retail, via: [:get], on: :collection
    match "/wholesale" => "orders#wholesale", as: :wholesale, via: [:get], on: :collection
    match "/return" => "orders#return", as: :return, via: [:get, :post]
  end

  resources :members, only: [:new, :create, :edit, :update]
  resources :lenders, only: [:new, :create, :edit, :update]
  resources :organizations, only: [:new, :create, :edit, :update]
  resources :customers do
    collection { post :import}
    get :autocomplete_customer_full_name, on: :collection

    match "/info" => "customers#info", as: :info, via: [:get], on: :member
    match "/purchases" => "customers#purchases", as: :purchases, via: [:get], on: :member
    match "/account_details" => "customers#account_details", as: :account_details, via: [:get], on: :member
    match "/payments" => "customers#payments", as: :payments, via: [:get], on: :member
    match "/advances" => "customers#advances", as: :advances, via: [:get], on: :member
    match "/advance_payments" => "customers#advance_payments", as: :advance_payments, via: [:get], on: :member

    resources :line_items, only: [:index], module: :customers do
      match "/scope_to_date" => "line_items#scope_to_date",  via: [:get], on: :collection, module: :customers
    end
    resources :advances, only: [:new, :create], module: :customers do
      match "/scope_to_date" => "advances#scope_to_date",  via: [:get], on: :collection, module: :customers
    end
    resources :advance_payments, only: [:new, :create], module: :customers do
      match "/scope_to_date" => "advance_payments#scope_to_date",  via: [:get], on: :collection, module: :customers
    end
    resources :payments, only: [:new, :create], module: :customers do
      match "/scope_to_date" => "payments#scope_to_date",  via: [:get], on: :collection, module: :customers
    end

    match "/export" => "customers#export", as: :export, via: [:get], on: :collection
  end
  
  resources :payables, only: [:index]
  resources :suppliers, :except => [:destroy] do
    get :autocomplete_supplier_owner, on: :collection
    resources :payments, only: [:new, :create], module: :suppliers
    match "/cash_stocks" => "suppliers#cash_stocks", as: :cash_stocks, via: [:get], on: :member
    match "/credit_stocks" => "suppliers#credit_stocks", as: :credit_stocks, via: [:get], on: :member
    match "/credit_payments" => "suppliers#credit_payments", as: :credit_payments, via: [:get], on: :member

  end
  resources :reports, only: [:index]
  resources :settings, only: [:index]
  resources :credits, only: [:index]
  resources :available_products, only: [:index]
  resources :low_stock_products, only: [:index]
  resources :out_of_stock_products, only: [:index]
  resources :expired_products, only: [:index]
  resources :one_month_expiry_products, only: [:index]
  resources :cash_advances, only: [:index]

  resources :wholesales do
    match "/botika" => "wholesales#botika", via: [:get], on: :collection
    match "/pharmacy" => "wholesales#pharmacy", via: [:get], on: :collection
  end
  resources :stocks, only: [:index, :show, :new, :create, :destroy] do
    collection { post :import}
    get :autocomplete_stock_name, on: :collection
    match "/scope_to_date" => "stocks#scope_to_date",  via: [:get], on: :collection
    match "/expired" => "stocks#expired", via: [:get], on: :collection
    match "/out_of_stock" => "stocks#out_of_stock", via: [:get], on: :collection
    match "/export" => "stocks#export", as: :export, via: [:get], on: :collection
  end

  namespace :wholesales do
    resources :line_items
    resources :orders
  end
  resources :businesses
  resources :users, only: [:show, :edit, :update] do 
    match "/info" => "users#info", as: :info, via: [:get], on: :member
    match "/activities" => "users#activities", as: :activities, via: [:get], on: :member
  end
  resources :employees, only: [:new, :create]
  resources :accounting, only: [:active_accounts, :inactive_accounts] do
    get :autocomplete_account_name, on: :collection
    match "/active_accounts" => "accounting#active_accounts", as: :active_accounts, via: [:get], on: :collection
    match "/inactive_accounts" => "accounting#inactive_accounts", as: :inactive_accounts, via: [:get], on: :collection
  end
  namespace :accounting do
    resources :balance_sheets, only:[:index, :scope_to_date] do
      match "/scope_to_date" => "balance_sheets#scope_to_date", via: [:get], on: :collection
    end
    resources :income_statements, only:[:index, :scope_to_date] do
      match "/scope_to_date" => "income_statements#scope_to_date", via: [:get], on: :collection
    end
    resources :dashboard, only: [:index]
    resources :reports, only:[:index]
    resources :accounts do
      get :autocomplete_accounting_account_name, on: :collection
      match "/activate" => "accounts#activate", via: [:post], on: :member
      match "/deactivate" => "accounts#deactivate", via: [:post], on: :member
    end
    resources :assets, controller: 'accounts', type: 'Accounting::Asset'
    resources :liabilities, controller: 'accounts', type: 'Accounting::Liability'
    resources :equities, controller: 'accounts', type: 'Accounting::Equity'
    resources :revenues, controller: 'accounts', type: 'Accounting::Revenue'
    resources :expenses, controller: 'accounts', type: 'Accounting::Expense'
    resources :fund_transfers, only: [:new, :create]
    resources :remittances, only: [:new, :create]
    resources :payables, only: [:new, :create]
    resources :disbursements, only: [:new, :create]
    resources :advances

    resources :entries do
      get :autocomplete_entry_description, on: :collection
      match "/scope_to_date" => "entries#scope_to_date", as: :scope_to_date, via: [:get], on: :collection
    end
  end

  namespace :owner do
    resources :dashboard, only: [:index]
  end
  resources :warranties, only: [:new, :create]
  resources :birs
  resources :retail_pricelists, only: [:index]
  resources :wholesale_pricelists, only: [:index]
end
