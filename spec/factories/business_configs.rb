FactoryBot.define do
  factory :business_config do
    include_wholesales { false }
  end
end
