source 'https://rubygems.org'
ruby '2.7.2'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'rails', '5.2.2.1'
gem 'bootsnap', require: false
gem 'sass-rails', '>= 6'
gem 'bootstrap-sass', '~> 3.4.1'
gem 'font-awesome-sass', '~> 5.15.1'
gem 'pg', '0.21'
gem 'puma', '~> 3.7', group: [:development, :production]
gem "animate-rails"
gem 'uglifier'
gem 'coffee-rails'
gem 'jbuilder'
gem 'redis'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'rails-jquery-autocomplete'
gem 'simple_form'
gem 'kaminari'
gem 'pg_search'
gem 'devise'
gem 'bootstrap-datepicker-rails'
gem 'momentjs-rails', '>= 2.9.0'
gem 'moment_timezone-rails'
gem 'pundit'
gem "select2-rails"
gem 'public_activity'
gem 'prawn'
gem 'prawn-table'
gem 'prawn-icon'
gem 'prawn-print'
gem 'roo', "~> 2.7.0"
gem 'roo-xls'
gem "spreadsheet"
gem 'axlsx', '2.1.0.pre'
gem 'axlsx_rails'
gem 'mina-puma', :require => false
gem 'whenever', :require => false
gem 'delayed_job_active_record'
gem 'bootstrap-popover-rails'
gem 'mini_magick'
gem 'barby'

group :development, :test do
  gem 'rspec-rails', '~> 3.8.0'
  gem 'rubocop', require: false
  gem 'rubocop-rspec'
  gem 'capybara'
  gem 'factory_bot_rails'
  gem 'faker'
end

group :development do
  gem 'listen'
  gem 'spring'
  gem 'spring-watcher-listen'
  gem 'spring-commands-rspec'
  gem 'bullet'
  gem 'pry-rails'
end

group :test do
  gem 'shoulda-matchers', '4.0.0.rc1'
  gem 'rails-controller-testing'
  gem 'database_rewinder'
  gem 'pdf-inspector', require: "pdf/inspector"
end