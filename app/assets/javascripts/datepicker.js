$(document).ready(function() {
  $('.datepicker').datepicker({
  	format: "MM dd, yyyy",
    orientation: "auto bottom",
    todayHighlight: 'true',
    autoclose: 'true'
  });
});

$(document).ready(function() {
  $('.datepicker-reporting').datepicker({
  	format: "MM dd, yyyy",
    orientation: "auto bottom",
    todayHighlight: 'true',
    autoclose: 'true'
  });
});