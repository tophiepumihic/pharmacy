class InvoicePdf < Prawn::Document
  TABLE_WIDTHS = [43, 36, 54, 54, 216, 43, 43, 65]
  HEADING_WIDTHS = [136, 300, 136]
  CUSTOMER_DETAILS_WIDTHS = [70, 300, 55, 20, 90, 22]
  def initialize(order, line_items, view_context)
    super(margin: [20, 20, 40, 20], page_size: [599, 792], page_layout: :portrait)
    # margin top: 198
    # half-bond: 396
    # height less margin: 712
    # width less margin: 559
    # header height exclude margin: 
    #@logo = image "#{Rails.root}/app/assets/images/logo_default.png", :at => [156,350], :width => 30
    @order = order
    @line_items = line_items
    @view_context = view_context
    business_details
    customer_details
    display_orders_table
    footer_for_warranty
  end
  
  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end

  def business_details
    bounding_box([0,730], :width => 559, :height => 100) do
      
    end
    # bounding_box([0,336], :width => 180, :height => 80) do
    #   @logo
    # end
    # bounding_box([181,356], :width => 210, :height => 80) do
    #   text "#{Business.last.try(:name)}", size: 12, style: :bold, align: :center
    #   text "Generic & Branded Drug Distributor", size: 10, align: :center
    #   text "Lumingay St. Pob. North", size: 10, align: :center
    #   text "Lagawe, Ifugao", size: 10, align: :center
    #   move_down 13
    #   text "QUOTE FORM", size: 14, style: :bold, align: :center
    # end
    # bounding_box([391,356], :width => 181, :height => 80) do
    #   move_down 14
    #   text "Mobile Nos.:", size: 11, style: :bold, align: :right
    #   text "09175830163", size: 10, align: :right
    #   text "09778353160", size: 10, align: :right
    #   move_down 5
    #   text "No. #{@order.invoice_number.try(:number)}", :color => "ED0000", size: 20, align: :right
    # end
  end

  def customer_details
    table(customer_details_data, cell_style: { :padding => [2,0,0,2], size: 10, font: "Helvetica", inline_format: true}, column_widths: CUSTOMER_DETAILS_WIDTHS) do
        cells.borders = []
        column(4).align = :center
        column(5).align = :right
    end
    move_down 50
  end
  def customer_details_data
    @customer_details_data ||=  [["", @order.customer.try(:full_name), "", "", @order.date.strftime("%B %e"), @order.date.strftime("%y")]]
  end

  def display_orders_table
    if @line_items.blank?
      move_down 10
      text "No Items data.", align: :center
    else
      # move_down 5
      # header = [["Quantity", "Unit", "Lot No.", "Expiry Date", "Description", "Unit Price", "Discount", "AMOUNT"]]
      # table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
      #   cells.borders = []
      #   row(0).font_style = :bold
      #   column(0).align = :right
      #   column(5).align = :right
      #   column(6).align = :right
      #   column(7).align = :right
      # end
      # stroke_horizontal_rule
      header = ["", "", "", "", "", "", "", ""]
      footer = ["", "", "", "", "", "", "", ""]
      line_items_data = @line_items.map { |e| 
        [e.quantity, e.stock.product.unit, 
          e.stock.lot_number, e.stock.expiry_date.try(:strftime, "%m/%d/%Y"), 
          e.stock.product.try(:name_and_description).first(42), price(e.unit_price), 
          "", price(e.total_price)]
      }

      table_data = [header, *line_items_data, footer]
      table(table_data, cell_style: { size: 8.5, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        row(0).font_style = :bold
        column(0).align = :center
        column(6).align = :right
        column(7).align = :right
      end
    end
  end

  def footer_for_warranty
    bounding_box([0, 118], :width => 554, :height => 118) do
      total
    end
    # move_down 3
    # bounding_box([0, 65], :width => 572, :height => 65) do
    #   stroke_horizontal_rule
    #   total
    #   move_down 3
    #   stroke_horizontal_rule
    #   bounding_box([0, 55], :width => 286, :height => 55) do
    #     move_down 9
    #     text "Prices are subject to change without prior notice.", size: 10, style: :bold
    #     move_down 24
    #     text "Prepared by: ____________________________", size: 10
    #   end
    #   bounding_box([286, 55], :width => 286, :height => 55) do
    #     move_down 9
    #     text 'Received above merchandise in good order and condition.', size: 10, style: :bold, align: :right
    #     move_down 24
    #     text "By: ____________________________", size: 10, align: :right
    #   end
    # end
  end

  def total
    table(total_data, cell_style: { :padding => [2,0,0,2], size: 10, font: "Helvetica", inline_format: true}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        column(4).align = :right
        column(0).font_style = :bold
        column(7).font_style = :bold
        column(6).font_style = :bold
        column(7).align = :right
    end
  end

  def total_data
    @total_data ||=  [["", "", "", "", "", "", price(@order.discount.amount), price(@order.total_amount_less_discount)]]
  end

end
