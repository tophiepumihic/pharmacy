class CustomersPdf < Prawn::Document
  TABLE_WIDTHS = [300, 272 ]
  def initialize(members, view_context)
    super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
    @members = members
    @view_context = view_context
    heading
    customers_table
  end
  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end
  def heading
    text "#{Business.last.try(:name)}", align: :center, size: 11, style: :bold
    text "#{Business.last.address}", align: :center, size: 10
    move_down 10
    text 'MEMBER TOTAL PURCHASES REPORT', size: 11, align: :center, style: :bold
    move_down 2
    text "#{Time.now.beginning_of_year.strftime("%B")} - #{Time.now.end_of_year.strftime("%B")} #{Time.now.end_of_year.strftime("%Y")}", size: 9, align: :center
    move_down 4
    stroke_horizontal_rule
    move_down 10
  end

  def customers_table
    if @members.blank?
      move_down 10
      text "No members data.", align: :center
    else
      move_down 10
      header = [["MEMBER", "TOTAL PURCHASE"]]
      table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        row(0).font_style = :bold
        column(1).align = :right
      end
      stroke_horizontal_rule
      header = ["", ""]
      footer = ["", ""]
      customers_data = @members.map { |e| [e.fullname, price(e.annual_cash_transactions)]}
      table_data = [header, *customers_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(1).align = :right
      end

      
    end
  end
end
