module Customers
  class LineItemsPdf < Prawn::Document
    TABLE_WIDTHS = [90, 220, 60, 70, 60, 72]
    HEADING_WIDTHS = [200,100,100,100]
    def initialize(customer, orders, from_date, to_date, view_context)
      super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
      @customer = customer
      @line_items = @customer.line_items
      @orders = orders
      @from_date = from_date
      @to_date = to_date
      @view_context = view_context
      heading
      display_cash_line_items_table
      display_credit_line_items_table
    end
    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end
    def heading
      text "#{Business.last.try(:name)}", size: 11, align: :center, style: :bold
      text "#{Business.last.address}", size: 9, align: :center
      text "TIN: #{Business.last.try(:tin)}", size: 9, align: :center
      text "#{Business.last.try(:mobile_number)}  #{Business.last.try(:email)}", size: 8, align: :center
      move_down 10
      text "CUSTOMER TRANSACTION REPORT", size: 11, align: :center, style: :bold
      move_down 1
      if @from_date.strftime('%B %e, %Y') == @to_date.strftime('%B %e, %Y')
        text "#{@from_date.strftime('%B %e, %Y')}", align: :center, size: 10
      else
        text "#{@from_date.strftime('%B %e, %Y')} - #{@to_date.strftime('%B %e, %Y')}", align: :center, size: 10
      end
      stroke_horizontal_rule
      move_down 5
      text "SUMMARY", style: :bold, size: 10
      table(heading_data, header: true, cell_style: { size: 10, font: "Helvetica", :padding => [3,3,0,0]}, column_widths: HEADING_WIDTHS) do
        cells.borders = []
      end
      move_down 10
      stroke_horizontal_rule
    end
    def heading_data
      [["#{@customer.full_name}", "", "Cash Transaction:", "#{price(@customer.total_cash_transactions)}"]] +
      [["#{@customer.address_details || "Address: N/A"}", "", "Credit Transaction:", "#{price(@customer.total_credit_transactions)}" ]] +
      [["#{@customer.mobile}", "", "Total Payment:", "#{price(@customer.total_payment)}" ]] +
      [["", "", "Remaining Balance:", "#{price(@customer.total_remaining_balance)}" ]] +
      [["", "", "Total Discount:", "#{price(@customer.total_discount)}" ]]

    end

    def display_cash_line_items_table
      move_down 10
      text "CASH TRANSACTIONS", size: 10, style: :bold
      move_down 5
      if @orders.cash.blank?
        text "No transactions.", align: :center
      else
        header = [["", "ITEM", "PRICE", "AMOUNT", "DISCOUNT", "TOTAL"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          column(1).align = :center
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
        end
        stroke_horizontal_rule
        move_down 5
        @orders.cash.each do |order|
          header = [order.date.strftime("%B %e, %Y"), "##{order.invoice_number.number}", "", "", price(order.discount.amount), price(order.total_amount_less_discount)]
          footer = ["", "", "", "", "", ""]
          line_items_data = order.line_items.map { |e| [ 
            "#{e.quantity.to_i} #{e.stock.product.unit}", 
            e.stock.product.name_and_description, 
            price(e.unit_price), 
            price(e.total_price),
            "",
            ""
          ]}

          table_data = [header, *line_items_data, footer]
          table(table_data, cell_style: { size: 9, font: "Helvetica", align: :center, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
            cells.borders = [:top]
            row(0).font_style = :bold
            column(1).align = :center
            column(2).align = :right
            column(3).align = :right
            column(4).align = :right
            column(5).align = :right
          end
        end
        move_down 10
      end
      stroke_horizontal_rule
    end

    def display_credit_line_items_table
      move_down 10
      text "CREDIT TRANSACTIONS", size: 10, style: :bold
      move_down 5
      if @orders.credit.blank?
        text "No transactions.", align: :center
      else
        header = [["", "ITEM", "PRICE", "AMOUNT", "DISCOUNT", "TOTAL"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          column(1).align = :center
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
        end
        stroke_horizontal_rule
        move_down 5
        @orders.credit.each do |order|
          header = [order.date.strftime("%B %e, %Y"), "##{order.invoice_number.number}", "", "", price(order.discount.amount), price(order.total_amount_less_discount)]
          footer = ["", "", "", "", "", ""]
          line_items_data = order.line_items.map { |e| [  
            "#{e.quantity} #{e.stock.product.unit}", 
            e.stock.product.name_and_description, 
            price(e.unit_price), 
            price(e.total_price),
            "",
            ""
          ]}
          table_data = [header, *line_items_data, footer]
          table(table_data, cell_style: { size: 9, font: "Helvetica", align: :center, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
            cells.borders = [:top]
            row(0).font_style = :bold
            row(0).align = :center
            column(1).align = :center
            column(2).align = :right
            column(3).align = :right
            column(4).align = :right
            column(5).align = :right
          end
        end
        move_down 10
      end
    end
  end
end
