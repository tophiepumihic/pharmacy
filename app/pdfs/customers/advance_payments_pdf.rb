module Customers
  class AdvancePaymentsPdf < Prawn::Document
    TABLE_WIDTHS = [90, 230, 70, 70, 72]
    HEADING_WIDTHS = [200,100,100,100]
    def initialize(member, advance_payments, from_date, to_date, view_context)
      super(margin: 40, page_size: [612, 1008], page_layout: :portrait)
      @member = member
      @from_date = from_date
      @to_date = to_date
      @advance_payments = advance_payments
      @view_context = view_context
      heading
      payments_table
    end
    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end
    def heading
      text "#{Business.last.try(:name)}", size: 11, align: :center, style: :bold
      text "#{Business.last.address}", size: 9, align: :center
      text "TIN: #{Business.last.try(:tin)}", size: 9, align: :center
      text "#{Business.last.try(:mobile_number)}  #{Business.last.try(:email)}", size: 8, align: :center
      move_down 10
      text "CASH ADVANCE PAYMENTS REPORT", size: 11, align: :center, style: :bold
      move_down 1
      if @from_date.strftime('%B %e, %Y') == @to_date.strftime('%B %e, %Y')
        text "#{@from_date.strftime('%B %e, %Y')}", align: :center, size: 10
      else
        text "#{@from_date.strftime('%B %e, %Y')} - #{@to_date.strftime('%B %e, %Y')}", align: :center, size: 10
      end
      stroke_horizontal_rule
      move_down 5
      text "SUMMARY", style: :bold, size: 10
      table(heading_data, header: true, cell_style: { size: 10, font: "Helvetica", :padding => [3,3,0,0]}, column_widths: HEADING_WIDTHS) do
        cells.borders = []
      end
      move_down 10
      stroke_horizontal_rule
    end
    def heading_data
      [["#{@member.full_name}", "", "Total Credit:", "#{price(@member.total_cash_advance)}" ]] +
      [["#{@member.address_details || "Address: N/A"}", "", "Total Payment:", "#{price(@member.total_cash_advance_payment)}" ]] +
      [["#{@member.mobile}", "", "Remaining Balance:", "#{price(@member.total_cash_advance_balance)}" ]]

    end

    def payments_table
      if @advance_payments.blank?
        move_down 10
        text "No payments data.", align: :center
      else
        move_down 10
        header = [["DATE", "REFERENCE NUMBER", "", "", "AMOUNT"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          column(1).align = :center
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
        end
        stroke_horizontal_rule
        header = ["", "", "", "", ""]
        footer = ["", "", "", "", ""]
        payments_data = @advance_payments.map { |e| [ 
          e.date.strftime("%B %e, %Y"), 
          e.reference_number, 
          "",
          "",
          price(e.credit_amounts.last.amount)
          ]}
        table_data = [header, *payments_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(1).align = :center
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
        end
      end
    end
  end
end
