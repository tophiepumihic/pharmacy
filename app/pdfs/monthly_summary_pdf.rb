class MonthlySummaryPdf < Prawn::Document
  TABLE_WIDTHS = [25, 65, 70, 70, 65, 65, 70, 70, 72 ]
  def initialize(stocks, orders, month_of, view_context)
    super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
    @orders = orders
    @stocks = stocks
    @from_date = month_of.beginning_of_month
    @to_date = month_of.end_of_month
    @view_context = view_context

    @sales = Accounting::Account.find_by_name("Sales")
    @cash_on_hand = Accounting::Account.find_by_name("Cash on Hand")
    @credits = Accounting::Account.find_by_name('Accounts Receivables Trade - Current')
    @payables = Accounting::Account.find_by_name('Accounts Payable-Trade')
    @purchases = Accounting::Account.find_by_name("Merchandise Inventory")
    @cost_of_goods_sold  = Accounting::Account.find_by_name("Cost of Goods Sold")
    @spoilage = Accounting::Account.find_by_name("Spoilage, Breakage and Losses")
    @expenses = Accounting::Expense.where.not(id: @cost_of_goods_sold.id).where.not(id: @spoilage.id).all
    @revenues = Accounting::Revenue.all
    first_entry = Accounting::Entry.order('date ASC').first if Accounting::Entry.any?
    @oldest_entry = first_entry ? first_entry.date: Time.zone.now
    heading
    display_orders_table

  end
  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end
  def heading
    text "#{Business.last.try(:name)}", align: :center, size: 11, style: :bold
    text "#{Business.last.address}", align: :center, size: 10
    move_down 10
    text 'SUMMARY REPORT', size: 12, align: :center, style: :bold
    text "#{@to_date.strftime("%B")}, #{@to_date.strftime("%Y")}", size: 9, align: :center
    move_down 3
    stroke_horizontal_rule
  end

  def previous_credits
    if Accounting::Entry.any?
      @credits.balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day})
    end
  end

  def current_credits
    if Accounting::Entry.any?
      @credits.balance({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
    end
  end

  def expenses
    if Accounting::Entry.any?
      @expenses.active_balance({from_date: @from_date, to_date: @to_date})
    end
  end

  def cost_of_goods_sold
    if Accounting::Entry.any?
      @cost_of_goods_sold.balance({from_date: @from_date, to_date: @to_date})
    end
  end

  def total_sales
    if Accounting::Entry.any?
      sales = @sales.balance({from_date: @from_date, to_date: @to_date})
      credits = @credits.debits_balance({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
      sales
    end
  end

  def total_purchases
    if Accounting::Entry.any?
      @purchases.debits_balance({from_date: @from_date, to_date: @to_date})
    end
  end

  def previous_cash
    if Accounting::Entry.any?
      @cash_on_hand.debits_balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day})
    end
  end

  def current_cash
    if Accounting::Entry.any?
      @cash_on_hand.balance({from_date: @oldest_entry, to_date: @to_date.end_of_day})
    end
  end

  def payables_beginning
    if Accounting::Entry.any?
      @payables.balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day})
    end
  end

  def payables_ending
    if Accounting::Entry.any?
      @payables.balance({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
    end
  end

  def starting_inventory
    if Accounting::Entry.any?
      @purchases.debits_balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day}) - 
      @purchases.credits_balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day})
    end
  end

  def ending_inventory
    if Accounting::Entry.any?
      @purchases.balance({from_date: @oldest_entry, to_date: @to_date.end_of_day})
    end
  end

  def starting_balance
    @cash_on_hand.balance({from_date: @oldest_entry, to_date: @from_date.yesterday.end_of_day})
  end

  def outstanding_balance
    @cash_on_hand.balance({from_date: @oldest_entry, to_date: @to_date.end_of_day})
  end

  def net_surplus 
    Accounting::Revenue.active_balance({from_date: @from_date, to_date: @to_date}) - 
    Accounting::Expense.where.not(id: @spoilage.id).active_balance({from_date: @from_date, to_date: @to_date})
  end

  def current_cash_purchases(e)
    @stocks.cash.created_between({from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day}).sum(:total_cost)
  end

  def current_payables(e)
    @payables.debits_balance({from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day})
  end

  def current_cash_sales(e)
    @orders.cash.created_between(from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day).total_amount_less_discount
  end

  def current_credit_sales(e)
    @credits.debits_balance({from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day})
  end

  def current_payments(e)
    @credits.credits_balance({from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day})
  end

  def current_expenses(e)
    @expenses.balance({from_date: e.to_date.beginning_of_day, to_date: e.to_date.end_of_day})
  end

  def current_balance(e)
    @cash_on_hand.balance({from_date: @oldest_entry.beginning_of_day, to_date: e.end_of_day})
  end

  def display_orders_table
    if Accounting::Entry.blank?
      move_down 10
      text "No Transactions data.", align: :center
    else
      move_down 10
      table_title = [["Cash on Hand (Beginning): ", "#{(price starting_balance)}", "", "Beginning Inventory: ", "#{(price starting_inventory)}"],
                  ["Cash on Hand (Ending): ", "#{(price outstanding_balance)}", "", "Ending Inventory: ", "#{(price ending_inventory)}"],
                  ["Sales: ", "#{(price total_sales)}", "", "Purchases: ", "#{(price total_purchases)}"],
                  ["Accounts Receivables (Beginning): ", "#{(price previous_credits)}", "", "Expenses: ", "#{(price expenses)}"],
                  ["Accounts Receivables (Ending): ", "#{(price current_credits)}", "", "Cost of Goods Sold", "#{(price cost_of_goods_sold)}" ],
                  ["Accounts Payables (Beginning): ", "#{(price payables_beginning)}", "", "", "" ],
                  ["Accounts Payables (Ending): ", "#{(price payables_ending)}", "", "Net Income:", "#{price(net_surplus)}" ]]
      table(table_title, :cell_style => {size: 9, :padding => [1, 1, 1, 1]}, column_widths: [160, 100, 100, 110, 102]) do
        cells.borders = []
        column(1).font_style = :bold
        column(4).font_style = :bold
        column(1).align = :right
        column(4).align = :right
        row(0).background_color = 'DDDDDD'
        row(1).background_color = 'DDDDDD'
        row(2).background_color = 'DDDDDD'
        row(3).background_color = 'DDDDDD'
        row(4).background_color = 'DDDDDD'
        row(5).background_color = 'DDDDDD'
        row(6).background_color = 'DDDDDD'
      end
      move_down 5
      table(table_data, header: false, cell_style: { size: 8, padding: 3}, column_widths: TABLE_WIDTHS) do
        row(0).font_style = :bold
      # /  row(0).background_color = 'DDDDDD'
        column(1).align = :right
        column(2).align = :right
        column(3).align = :right
        column(4).align = :right
        column(5).align = :right
        column(6).align = :right
        column(7).align = :right
        column(8).align = :right

        row(-1).font_style = :bold
        row(-1).size = 11

      end
    end
  end

  def table_data
    move_down 5
    
    [["Date", "Cash Purchases", "Payables Payments", "Cash Sales", "Credit Sales", "AR Collection", "Operational Expenses", "Total Cash Collection", "Balance"]] +
    @table_data ||= (@from_date.to_date..@to_date.to_date).map { |e| 
      if Accounting::Entry.entered_on({from_date: e.to_date.yesterday.end_of_day, to_date: e.to_date.end_of_day}).any?
        [
          e.strftime("%d"), 
        price(current_cash_purchases(e)),
        price(current_payables(e)),
        price(current_cash_sales(e)), 
        price(current_credit_sales(e)), 
        price(current_payments(e)),
        price(current_expenses(e)),
        price(current_cash_sales(e) + current_payments(e) - current_expenses(e)),
        price(current_balance(e))
        ]
      else
        [e.strftime("%d"), "_", "_", "_", "_", "_", "_", "_", "_"]
      end
    } +
    [["", "", "", "", "", "", "", "", ""]]

  end
end
