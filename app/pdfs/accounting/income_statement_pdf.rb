module Accounting
  class IncomeStatementPdf < Prawn::Document
  TABLE_WIDTHS = [50, 350, 152, 20]
  def initialize(scope, revenues,  expenses, from_date, to_date, view_context)
    super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
    @revenues = revenues
    @expenses = expenses
    @from_date = from_date
    @to_date = to_date
    @scope = scope
    @view_context = view_context
    heading
    display_revenues
    display_expenses
    net_surplus
  end

  def price(number)
    @view_context.number_to_currency(number, :unit => "P ")
  end


  def heading
    text "#{Business.last.name}", style: :bold, size: 11, align: :center
    text "#{Business.last.address}", size: 10, align: :center
    move_down 15
    text 'INCOME STATEMENT', size: 11, align: :center, style: :bold
    if @scope.present?
      if @scope == "Daily"
        text "#{@to_date.strftime("%B %e, %Y")}", size: 10, align: :center
      elsif @scope == "Monthly"
        text "#{@to_date.strftime("%B, %Y")}", size: 10, align: :center
      elsif @scope == "Annually"
        text "#{@from_date.strftime("%B")} - #{@to_date.strftime("%B, %Y")}", size: 10, align: :center
      end
    else
      text "#{@from_date.strftime("%B %e, %Y")} - #{@to_date.strftime("%B %e, %Y")}", size: 10, align: :center
    end
    move_down 5
    stroke_horizontal_rule
    end

    def display_revenues
      move_down 10
      text "REVENUES", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>TOTAL REVENUES</b>", "<b>#{price(Accounting::Revenue.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day))}</b>", ""]
      assets_data ||= Accounting::Revenue.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    def display_expenses
      move_down 15
      text "EXPENSES", size: 11
      header = ["", "", "", ""]
      footer = ["", "<b>TOTAL EXPENSES</b>", "<b>#{price(@expenses.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day))}</b>", ""]
      assets_data ||= @expenses.active_with_balance.map { |e| ["", e.name, price(e.balance(from_date: @from_date, to_date: @to_date)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    def net_surplus
      move_down 10
      table(net_surplus_data, header: false,  cell_style: { size: 10, font: "Helvetica", :inline_format => true, padding: [0,0,0,0] }, column_widths: TABLE_WIDTHS) do
        cells.borders = []
        column(2).align = :right
        row(0).font_style = :bold
      end
    end

    def net_surplus_data
      stroke_horizontal_rule
      move_down 5
      @net_surplus_data ||= [["", "NET INCOME", price(@revenues.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day) - @expenses.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)), ""]]
    end

  end
end
