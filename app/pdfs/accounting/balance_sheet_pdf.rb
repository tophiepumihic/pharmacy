module Accounting
  class BalanceSheetPdf < Prawn::Document
    TABLE_WIDTHS = [50, 350, 152, 20]
    EXPENSE_TABLE_WIDTHS = [100, 350, 102, 20]
    def initialize(scope, revenues, liabilities, assets, expenses, from_date, to_date, view_context)
      super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
      @liabilities = liabilities
      @assets = assets
      @expenses = expenses
      @revenues = revenues
      @from_date = from_date
      @to_date = to_date
      @scope = scope
      @view_context = view_context
      balance_heading
      display_asset if @assets.present?
      display_liability if @liabilities.present?
      #display_balance
      # move_down 30
      # income_heading
      #display_revenue if @revenues.present?
      display_expense if @expenses.present?
      # display_income
    end
    def total_asset
      if @assets.present?
        @assets.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
      else
        0
      end
    end
    def total_liability
      if @liabilities.present?
        @liabilities.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day) +
        beginning_cash_on_hand + beginning_merchandise_inventory
      else
        0
      end
    end

    def beginning_cash_on_hand
      Accounting::Account.find_by(name: "Cash on Hand").balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.beginning_of_month.yesterday.end_of_day)
    end

    def beginning_merchandise_inventory
      Accounting::Account.find_by(name: "Merchandise Inventory").balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.beginning_of_month.yesterday.end_of_day)
    end

    def total_revenue
      if @revenues.present?
        if @scope == "Monthly"
          @revenues.active_balance(from_date: @to_date.beginning_of_month.yesterday.end_of_day, to_date: @to_date.end_of_day)
        else
          @revenues.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
        end
      else
        0
      end
    end

    def total_expense
      if @expenses.present?
        if @scope == "Monthly"
          @expenses.active_balance(from_date: @to_date.beginning_of_month.yesterday.end_of_day, to_date: @to_date.end_of_day)
        else
          @expenses.active_balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)
        end
      else
        0
      end
    end

    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end

    def balance_heading
      text "#{Business.last.name}", style: :bold, size: 11, align: :center
      text "#{Business.last.address}", size: 10, align: :center
      move_down 15
      text 'BALANCE SHEET', size: 11, align: :center, style: :bold
      if @scope == "Monthly"
        text "#{@to_date.strftime("%B, %Y")}", size: 10, align: :center
      else
        text "#{@to_date.beginning_of_year.strftime("%B")} - #{@to_date.strftime("%B")}, #{@to_date.strftime("%Y")}", size: 10, align: :center
      end
      move_down 5
      stroke_horizontal_rule
    end

    def display_asset
      move_down 10
      text "ASSETS", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      footer = ["", "<b>TOTAL</b>", "<b>#{price(total_asset)}</b>", ""]
      assets_data ||= Accounting::Asset.active_with_entries.map { |e| ["", e.name, price(e.balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    #################################################################################

    def display_liability
      move_down 10
      text "LIABILITIES", size: 11
      stroke_horizontal_rule
      header = ["", "", "", ""]
      beginning_cash = ["", "Beginning Cash on Hand", "#{price(beginning_cash_on_hand)}", ""]
      beginning_inventory = ["", "Beginning Merchandise Inventory", "#{price(beginning_merchandise_inventory)}", ""]
      footer = ["", "<b>TOTAL</b>", "<b>#{price(total_liability)}</b>", ""]
      assets_data ||= Accounting::Liability.active_with_entries.map { |e| ["", e.name, price(e.balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)), ""]}
      table_data = [header, beginning_cash, beginning_inventory, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end

    ##############################################################################

    def display_expense
      move_down 10
      text "Accrued Expenses Breakdown", size: 10
      header = ["", "", "", ""]
      footer = ["", "", "", ""]
      assets_data ||= Accounting::Expense.active_with_entries.map { |e| ["", e.name, price(e.balance(from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day)), ""]}
      table_data = [header, *assets_data, footer]
      table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: EXPENSE_TABLE_WIDTHS) do
        cells.borders = [:top]
        row(0).font_style = :bold
        column(2).align = :right
      end
    end
  end
end
