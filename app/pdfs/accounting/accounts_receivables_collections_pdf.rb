module Accounting
  class AccountsReceivablesCollectionsPdf < Prawn::Document
    TABLE_WIDTHS = [100, 150, 120, 110, 92 ]
    def initialize(entries, from_date, to_date, view_context)
      super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
      @entries = entries
      @from_date = from_date
      @to_date = to_date
      @view_context = view_context
      heading
      customers_table
    end
    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end
    def heading
      text "#{Business.last.try(:name)}", align: :center, size: 11, style: :bold
      text "#{Business.last.address}", align: :center, size: 10
      move_down 10
      text 'AR COLLECTION REPORT', size: 11, align: :center, style: :bold
      move_down 2
      if @from_date.strftime('%B %e, %Y') == @to_date.strftime('%B %e, %Y')
        text "#{@from_date.strftime('%B %e, %Y')}", align: :center, size: 10
      else
        text "#{@from_date.strftime('%B %e, %Y')} - #{@to_date.strftime('%B %e, %Y')}", align: :center, size: 10
      end
      move_down 4
      stroke_horizontal_rule
      move_down 10
    end

    def customers_table
      if @entries.blank?
        move_down 10
        text "No members data.", align: :center
      else
        move_down 10
        header = [["DATE", "MEMBER", "PRINCIPAL PAYMENT", "INTEREST PAYMENT", "DISCOUNT"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
          column(5).align = :right
          column(6).align = :right
          column(7).align = :right
        end
        stroke_horizontal_rule
        header = ["", "", "", "", ""]
        footer = ["", "", "", "", ""]
        customers_data = @entries.map { |e| [ 
          e.date.strftime("%B %e, %Y"), 
          e.commercial_document.try(:full_name), 
          price(e.credit_amounts.first.amount),
          price(e.credit_amounts.second.amount),
          price(e.credit_amounts.third.amount),
          ]}
        table_data = [header, *customers_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(2).align = :right
          column(3).align = :right
          column(4).align = :right
        end
      end
    end
  end
end