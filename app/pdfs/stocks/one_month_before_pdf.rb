module Stocks
  class OneMonthBeforePdf < Prawn::Document
    TABLE_WIDTHS = [250, 70, 60, 100, 92]
    def initialize(stocks, view_context)
      super(margin: 20, page_size: [612, 1008], page_layout: :portrait)
      @stocks = stocks
      @view_context = view_context
      heading
      display_products_table

    end
    def price(number)
      @view_context.number_to_currency(number, :unit => "P ")
    end
    def heading
      text "#{Business.last.name}", style: :bold, size: 11, align: :center
      text "#{Business.last.address}", size: 10, align: :center
      move_down 15
      text 'Expiring Stocks', size: 11, align: :center, style: :bold
      move_down 3
      text "As of #{Time.zone.now.strftime("%B %e, %Y")}", size: 10, align: :center
      move_down 5
      stroke_horizontal_rule
    end
    def display_products_table
      if @stocks.blank?
        move_down 10
        text "No products data.", align: :center
      else
        move_down 10
        header = [["ITEM", "UNIT", "IN STOCK", "EXPIRY DATE", "REMAINING"]]
        table(header, :cell_style => {size: 9, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = []
          row(0).font_style = :bold
          row(0).align = :center
        end
        stroke_horizontal_rule
        header = ["", "", "", "", ""]
        footer = ["", "", "", "", ""]
        products_data = @stocks.map { |e| [
          e.product.name_and_description, 
          e.product.unit, 
          e.in_stock,
          e.expiry_date.strftime("%B %e, %Y"),
          "#{(Time.zone.now.to_date...e.expiry_date).to_a.size} days"
          ]}
        table_data = [header, *products_data, footer]
        table(table_data, cell_style: { size: 9, font: "Helvetica", inline_format: true, :padding => [2, 4, 2, 4]}, column_widths: TABLE_WIDTHS) do
          cells.borders = [:top]
          row(0).font_style = :bold
          column(1).align = :center
          column(2).align = :center
          column(3).align = :center
          column(4).align = :center
        end
      end
    end
  end
end
