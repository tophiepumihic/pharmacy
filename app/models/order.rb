class Order < ApplicationRecord
  include PgSearch
  pg_search_scope :text_search, :against => [:reference_number],
                  :associated_against => {:customer => [:full_name], :invoice_number => [:number]}

  has_one :official_receipt, as: :receiptable
  has_one :invoice_number, dependent: :destroy
  has_one :entry, class_name: "Accounting::Entry", as: :commercial_document, dependent: :destroy

  belongs_to :employee, foreign_key: 'employee_id'
  belongs_to :customer, foreign_key: 'customer_id'
  enum pay_type:[:cash, :credit]
  enum order_type: [:retail, :wholesale]
  has_many :line_items, dependent: :destroy
  has_many :stocks, through: :line_items
  has_one :discount, dependent: :destroy

  before_save :set_date, :set_customer
  accepts_nested_attributes_for :discount
  accepts_nested_attributes_for :entry

  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def set_to_paid!
    if cash?
      line_items.each do |line_item|
        line_item.paid = true
      end
    end
  end

  def self.cost_of_goods_sold
    all.to_a.sum{ |a| a.cost_of_goods_sold }
  end

  def cost_of_goods_sold
    line_items.cost_of_goods_sold
  end

  def self.income
    all.to_a.sum{ |a| a.income }
  end

  def income
    line_items.income - total_discount
  end
  
  def customer_name
    customer.try(:full_name)
  end

  def total_discount
    if discount.present?
      discount.amount
    else
      0
    end
  end

  def reference_number
    "#{id.to_s.rjust(8, '0')}"
  end

  def self.retail_and_wholesale
    self.includes(:customer, :invoice_number).order(date: :desc).all
  end

  def self.total_amount
    all.map{|a| a.total_amount }.sum
  end

  def self.total_amount_less_discount
    all.map{|a| a.total_amount_less_discount }.sum
  end

  def self.total_discount
    all.map{|a| a.total_discount }.sum
  end
  
  def total_amount
    line_items.sum(:total_price)
  end

  def total_amount_plus_discount
    total_amount + total_discount
  end

  def total_payment
    line_items.where(paid: true).sum(:total_price) + line_items.where(paid: false).sum {|l| l.payments.sum(:amount)}
  end

  def total_amount_less_discount
    total_amount - total_discount
  end

  def balance
    total_amount_less_discount - total_payment
  end

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def stock_cost
    line_items.map{|a| a.cost_of_goods_sold}.sum
  end

  def return_line_items_to_stock!
    self.line_items do |line_item|
      line_item.stock.update(quantity: line_item.quantity + line_item.stock.quantity)
    end
  end

  def update_stock_status!
    self.line_items.map {|l| l.stock.update(deleted: false)}
  end

  def remove_entry_for_return_order!
    Accounting::Entry.where(order_id: id).destroy_all
  end

  def create_entry_for_return_order!
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @sales_returns = Accounting::Account.find_by(name: "Sales Returns and Allowances")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
    if cash?
      Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Sales return of order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.total_amount_less_discount, account: @sales_returns},
        {amount: self.stock_cost, account: @merchandise_inventory}], 
      credit_amounts_attributes:[{amount: self.total_amount_less_discount, account: @cash_on_hand}, 
        {amount: self.stock_cost, account: @cost_of_goods_sold}],  
      employee_id: self.employee_id)
    elsif credit?
      Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Sales return of credit order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.total_amount_less_discount, account: @sales_returns},
        {amount: self.stock_cost, account: @merchandise_inventory}], 
      credit_amounts_attributes:[{amount: self.total_amount_less_discount, account: @accounts_receivable}, 
        {amount: self.stock_cost, account: @cost_of_goods_sold}],  
      employee_id: self.employee_id)
    end
  end

  def set_customer_has_credit_to_true!
    Customer.find(customer_id).update(has_credit: true)
  end

  def set_customer_has_credit_to_false!
    Customer.find(customer_id).update(has_credit: false)
  end

  def set_partial_payment!
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
    if self.credit? && cash_tendered.present? && change.present?
      Accounting::Entry.create(entry_type: "credit_payment", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Partial Payment of order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.cash_tendered, account: @cash_on_hand}], 
      credit_amounts_attributes:[{amount: self.cash_tendered, account: @accounts_receivable}],  
      employee_id: self.employee_id)
    end
  end

  def delete_stock_if_zero!
    line_items.each do |line_item|
      if line_item.stock.in_stock == 0
        line_item.stock.update!(deleted: true)
      end
    end
  end

  def create_entry
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @purchases = Accounting::Account.find_by(name: "Purchases")
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @sales = Accounting::Account.find_by(name: "Sales")
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")

    if self.cash? && !self.discounted?
      entry_for_cash

    elsif self.credit? && !self.discounted?
      entry_for_credit

    elsif self.cash? && self.discounted?
      entry_for_discounted_cash

    elsif self.credit? && self.discounted?
      entry_for_discounted_credit
    end
  end

  def entry_for_cash
    Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Cash Sales of order ##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.total_amount, account: @cash_on_hand},
        {amount: self.stock_cost, account: @cost_of_goods_sold}], 
      credit_amounts_attributes:[{amount: self.total_amount, account: @sales}, 
        {amount: self.stock_cost, account: @merchandise_inventory}],  
      employee_id: self.employee_id)
  end

  def entry_for_credit
    Accounting::Entry.create(entry_type: "credit_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Credit Sales of order##{self.reference_number}", 
      debit_amounts_attributes: [{amount: self.total_amount, account: @accounts_receivable}, 
        {amount: self.stock_cost, account: @cost_of_goods_sold}], 
      credit_amounts_attributes:[{amount: self.total_amount, account: @sales}, 
        {amount: self.stock_cost, account: @merchandise_inventory}], 
      employee_id: self.employee_id)
  end

  def entry_for_discounted_cash
    Accounting::Entry.create(entry_type: "cash_order", order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Cash Sales of order##{self.reference_number} with discount of #{self.total_discount}", 
      debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
        {amount: self.total_amount_less_discount, account: @cash_on_hand}], 
      credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory}, 
        {amount: self.total_amount_less_discount, account: @sales}], 
      employee_id: self.employee_id)
  end

  def entry_for_discounted_credit
    Accounting::Entry.create(order_id: self.id, commercial_document_id: self.customer_id, 
      commercial_document_type: self.customer.class, date: self.date, 
      description: "Credit Sales of order ##{self.reference_number} with discount of #{self.total_discount}", 
      debit_amounts_attributes: [{amount: self.stock_cost, account: @cost_of_goods_sold},
        {amount: self.total_amount_less_discount, account: @accounts_receivable}], 
      credit_amounts_attributes:[{amount: self.stock_cost, account: @merchandise_inventory},
        {amount: self.total_amount_less_discount, account: @sales}],  
      employee_id: self.employee_id)
  end

  private

  def ensure_not_referenced_by_line_items
    errors[:base] << "Order still referenced by line items" if self.line_items.present?
    return false 
  end

  def set_date
    if self.date.nil?
      self.date = Time.zone.now
    else
      if self.persisted?
        self.date = Time.new(self.date.year, self.date.month, self.date.day, created_at.hour, created_at.min, 0, Time.zone)
      else
        self.date = Time.new(self.date.year, self.date.month, self.date.day, Time.now.hour, Time.now.min, 0, Time.zone)
      end
    end
  end

  def set_customer
    if customer_id.nil?
      customer_id = Customer.find_by(first_name: 'Guest').id
    end
  end
end
