class Stock < ApplicationRecord
  include PgSearch
  require 'csv'

  pg_search_scope :search_by_name, :against => [:name, :serial_number, :wholesale_code]
  enum status:[:available, :low_stock, :out_of_stock, :expired]
  enum payment_type: [:cash, :credit]
  belongs_to :product
  belongs_to :employee
  belongs_to :supplier, optional: true
  has_one :entry, class_name: "Accounting::Entry", foreign_key: 'stock_id', dependent: :destroy
  has_many :line_items
  has_many :orders, through: :line_items
  has_many :refunds
  has_many :item_expenses

  validates :quantity, :supplier_id, :unit_cost, :total_cost,  presence: true, numericality: true
  before_save :set_date, :set_name

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      stock_hash = row.to_hash
      stock = Stock.where(id: stock_hash['id'])

      if stock.count == 1
        stock.first.update_attributes(stock_hash)
      else
        stock_create = Stock.create!(stock_hash)
        # stock_create.create_entry
        stock_create.set_stock_status_to_product
      end
    end
  end

  def to_s
    name
  end

  def self.total_cost
    all.order(date: :desc).sum(:total_cost)
  end

  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('date' => from_date..to_date)
    else
      all
    end
  end

  def self.total_expired_cost
    self.expired.sum{ |e| e.in_stock * e.unit_cost }
  end

  def self.expired
    all.order(:name).select{ |a| a.expired?}
  end

  def self.one_month_to_expire
    all.order(:name).select{ |s| s.one_month_to_expire?}
  end

  def self.ten_days_to_expire
    all.order(:name).select{ |s| s.ten_days_to_expire?}
  end

  def self.out_of_stock
    all.order(date: :desc).select{ |a| a.out_of_stock?}
  end

  def self.deleted
    all.order(date: :desc).select{ |a| a.deleted?}
  end

  def self.available
    all.order(date: :desc).select{ |a| !a.expired? || !a.out_of_stock? || !a.deleted?}
  end

  def self.unavailable
    all.order(date: :desc).select{ |a| a.expired? || a.out_of_stock? || a.deleted?}
  end

  def deleted?
    deleted == true
  end

  def one_month_to_expire?
    if expiry_date.present?
      Time.zone.now >= (expiry_date - 30.days) && !deleted?
    else
      false
    end
  end

  def expired?
    if expiry_date.present?
      Time.zone.now >= expiry_date && !deleted?
    else
      false
    end
  end

  def out_of_stock?
    in_stock.zero? || in_stock.negative?
  end

  def low_stock?
    in_stock <= product.stock_alert_count
  end

  def available?
    status == "available"
  end

  def unavailable?
    status != "available"
  end

  def in_stock
    quantity - sold - broken_lost_and_expired
  end

  def broken_lost_and_expired
    item_expenses.sum(:quantity)
  end

  def sold
    line_items.sum(:quantity)
  end

  def alert
    if low_stock? && !expired?
      "Low Stock"
    elsif expired?
      "Expired"
    elsif expired? && low_stock?
      "Expired"
    elsif out_of_stock?
      "Out of Stock"
    end
  end

  def set_status!
    if expired? && low_stock?
      self.expired!
    end
  end

  def in_stock_amount
    in_stock * unit_cost
  end

  def delete_all!
    self.out_of_stock.each do |s|
      s.update(deleted: true)
    end
  end

  def delete!
    if in_stock.zero?
      self.update(deleted: true)
    end
  end

  def create_expense_from_expired_stock!
    @spoilage_breakage_and_loses = Accounting::Account.find_by(name: "Spoilage, Breakage and Losses")
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")

    Accounting::Entry.create!(stock_id: self.id, commercial_document_id: self.id, 
      commercial_document_type: self.class, date: Time.zone.now, description: "Expired stock of #{self.in_stock} #{self.product.unit} #{self.product.name_and_description}", 
      debit_amounts_attributes: [amount: self.in_stock_amount, account: @spoilage_breakage_and_loses], 
      credit_amounts_attributes:[amount: self.in_stock_amount, account: @merchandise_inventory], 
      employee_id: self.employee_id)

    self.update(deleted: true)
  end

  def remove_expense_from_expired_stock!
    @entry = Accounting::Entry.where(commercial_document_id: self.id).where(commercial_document_type: self.class.name).where(description: "Expired stock of #{self.in_stock} #{self.product.unit} #{self.product.name_and_description}").last
    if @entry.present?
      @entry.destroy
    else
      false
    end
    self.update(deleted: false)
  end

  def update_entry
    if entry.present?
      @supplier_id = Supplier.find_by(business_name: "Old Stock").id
      unless @supplier_id == self.supplier_id
        @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
        @accounts_payable = Accounting::Account.find_by(name: "Accounts Payable-Trade")
        @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")

        #CASH PURCHASE ENTRY##
        if self.cash?
          entry.update(entry_type: "cash_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
            commercial_document_type: self.supplier.class, date: self.date, 
            description: "Cash Purchase of stocks", employee_id: self.employee_id)
          entry.debit_amounts.last.update!(amount: self.total_cost, account: @merchandise_inventory) 
          entry.credit_amounts.last.update!(amount: self.total_cost, account: @cash_on_hand)
          
        #Cedit PURCHASE ENTRY##
        elsif self.credit?
          entry.update(entry_type: "credit_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
            commercial_document_type: self.supplier.class, date: self.date, 
            description: "Credit Purchase of stocks", employee_id: self.employee_id) 
          entry.debit_amounts.last.update!(amount: self.total_cost, account: @merchandise_inventory) 
          entry.credit_amounts.last.update!(amount: self.total_cost, account: @accounts_payable)
        end
      else
        entry.destroy
      end
    else
      create_entry
    end
  end

  def create_entry
    @supplier_id = Supplier.find_by(business_name: "Old Stock").id
    unless @supplier_id == self.supplier_id
      @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
      @accounts_payable = Accounting::Account.find_by(name: "Accounts Payable-Trade")
      @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
      #CASH PURCHASE ENTRY##
      if self.cash?
        Accounting::Entry.create!(entry_type: "cash_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
          commercial_document_type: self.supplier.class, date: self.date, 
          description: "Cash Purchase of stocks", 
          debit_amounts_attributes: [amount: self.total_cost, account: @merchandise_inventory], 
          credit_amounts_attributes:[amount: self.total_cost, account: @cash_on_hand],  
          employee_id: self.employee_id)
        
      #Cedit PURCHASE ENTRY##
      elsif self.credit?
        Accounting::Entry.create!(entry_type: "credit_stock", stock_id: self.id, commercial_document_id: self.supplier.id, 
          commercial_document_type: self.supplier.class, date: self.date, 
          description: "Credit Purchase of stocks", 
          debit_amounts_attributes: [amount: self.total_cost, account: @merchandise_inventory], 
          credit_amounts_attributes:[amount: self.total_cost, account: @accounts_payable],  
          employee_id: self.employee_id)
      end
    end
  end

  private
  def set_date
    if self.date.nil?
      self.date = Time.zone.now
    else
      if self.persisted?
        self.date = Time.new(self.date.year, self.date.month, self.date.day, created_at.hour, created_at.min, 0, Time.zone)
      else
        self.date = Time.new(self.date.year, self.date.month, self.date.day, Time.now.hour, Time.now.min, 0, Time.zone)
      end
    end
  end
  def set_name
    self.name = self.product.name_and_description
  end
end
