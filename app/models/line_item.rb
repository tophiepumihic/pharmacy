class LineItem < ApplicationRecord
  belongs_to :itemable, polymorphic: true 
  belongs_to :stock, foreign_key: 'stock_id'
  belongs_to :cart
  belongs_to :order
  has_many :payments
  has_one :entry, class_name: "Accounting::Entry", as: :commercial_document, dependent: :destroy
  belongs_to :employee, foreign_key: 'user_id'
  belongs_to :customer, foreign_key: 'customer_id'
  enum pricing_type: [:retail, :botika, :pharmacy]
  scope :by_total_price, -> { all.to_a.sort_by(&:total_price) }
  
  validates :quantity, numericality: { less_than_or_equal_to: :stock_quantity }, on: :create
  delegate :name, to: :stock, prefix: true
  delegate :cash?, :credit?, to: :order

  def order_date
    order.date
  end
  
  def self.created_between(hash={})
    if hash[:from_date] && hash[:to_date]
      from_date = hash[:from_date].kind_of?(Time) ? hash[:from_date] : DateTime.parse(hash[:from_date].strftime('%Y-%m-%d 12:00:00 AM'))
      to_date = hash[:to_date].kind_of?(Time) ? hash[:to_date] : DateTime.parse(hash[:to_date].strftime('%Y-%m-%d 23:59:59 PM'))
      where('created_at' => from_date..to_date)
    else
      all
    end
  end

  def stock_quantity
    if stock
      self.stock.in_stock
    end
  end

  def self.cash
    all.select{|a| a.cash? }
  end

  def cash?
    order.present? && order.cash?
  end
  def self.credit
    all.select{|a| a.credit? }
  end
  def credit?
    order.present? && order.credit?
  end

  def cost_of_goods_sold
    stock.unit_cost * quantity
  end

  def income
    total_price - cost_of_goods_sold
  end

  def self.income
    all.to_a.sum { |item| item.income }
  end

  def self.cost_of_goods_sold
    all.to_a.sum { |item| item.cost_of_goods_sold }
  end

  def total_amount
    unit_price * quantity
  end

  def self.total_amount
    self.all.to_a.sum { |item| item.total_price }
  end

  def type
    if cash?
      "Cash"
    elsif credit?
      "Credit"
    end
  end

  def return_quantity_to_stock!
    self.stock.product.set_status!
  end

  def update_stock_status!
    self.stock.update(deleted: false)
  end
        
  def create_entry_for_sales_return!
    @sales = Accounting::Account.find_by(name: "Sales")
    @cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
    @merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
    @cost_of_goods_sold = Accounting::Account.find_by(name: "Cost of Goods Sold")
    @sales_returns = Accounting::Account.find_by(name: "Sales Returns and Allowances")
    @accounts_receivable = Accounting::Account.find_by(name: "Accounts Receivables Trade - Current")
    if self.cash?
      Accounting::Entry.create!(commercial_document_id: self.order.customer.id, 
        commercial_document_type: self.order.customer.class, date: Time.zone.now, 
        description: "Sales return of order ##{self.order.reference_number}, item: #{self.stock.try(:name)}, Quantity: #{self.quantity}", 
        debit_amounts_attributes: [{amount: self.total_price, account: @sales_returns}, {amount: self.cost_of_goods_sold, account: @merchandise_inventory}], 
        credit_amounts_attributes:[{amount: self.total_price, account: @cash_on_hand}, {amount: self.cost_of_goods_sold, account: @cost_of_goods_sold}], 
        employee_id: self.user_id)

    elsif self.credit?
      Accounting::Entry.create!(commercial_document_id: self.order.customer.id, 
        commercial_document_type: self.order.customer.class, date: Time.zone.now, 
        description: "Sales Return of order ##{self.order.reference_number}, item: #{self.stock.try(:name)}, Quantity: #{self.quantity}", 
        debit_amounts_attributes: [{amount: self.total_price, account: @sales_returns}, {amount: self.cost_of_goods_sold, account: @merchandise_inventory}], 
        credit_amounts_attributes:[{amount: self.total_price, account: @accounts_receivable}, {amount: self.cost_of_goods_sold, account: @cost_of_goods_sold}], 
        employee_id: self.user_id)
    end
  end

  def set_total_price!
    self.total_price = total_amount
  end

  private
  def ensure_quantity_is_available
    return false
  end
end
