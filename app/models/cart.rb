class Cart < ApplicationRecord
  belongs_to :employee
  has_many :line_items, dependent: :destroy
  has_many :stocks, through: :line_items

  def total_amount
    line_items.sum(:total_price)
  end
end
