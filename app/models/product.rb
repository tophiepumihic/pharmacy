class Product < ApplicationRecord
  include PublicActivity::Common
  include PgSearch
  require 'csv'

  pg_search_scope( :search_by_name, 
                    against: [:name_and_description, :name, :description],
                    :associated_against => {:barcodes => [:code]},
                    using: { tsearch: { prefix: true }} )

  has_many :barcodes
  has_many :stocks, dependent: :destroy
  has_many :refunds, through: :stocks
  has_many :line_items, through: :stocks
  has_many :orders, through: :line_items
  has_many :item_expenses
  validates :name, :retail_price, :botika_price, :pharmacy_price, :stock_alert_count, :unit, presence: true
  validate :zero_alert_count

  before_destroy :ensure_not_referenced_by_any_line_item
  before_save :set_name_description
  before_update :set_name_description

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      product_hash = row.to_hash
      product = Product.where(id: product_hash['id'])

      if product.count == 1
        product.first.update_attributes(product_hash)
      else
        Product.create!(product_hash)
      end
    end
  end
  
  def quantity
    stocks.sum(:quantity)
  end

  def in_stock
    if quantity <= 0
      0
    else
      quantity - sold - deleted_in_stock
    end
  end

  def deleted_in_stock
    stocks.where(deleted: true).sum {|s| s.quantity - s.sold}
  end

  def sold
    line_items.sum(:quantity)
  end

  def self.total_inventory_cost
    all.sum { |t| t.total_inventory_cost}
  end

  def total_stock_cost
    self.stocks.purchased.sum(:total_cost)
  end

  def total_inventory_cost
    stocks.sum {|s| (s.quantity-s.sold)*s.unit_cost}
  end

  def quantity_and_unit
    "#{quantity} #{unit}"
  end

  def name_description
    "#{name} #{description}"
  end

  def set_name_description
    if description.present?
      self.name_and_description = name_description.upcase
    else
      self.name_and_description = name.upcase
    end
  end

  def out_of_stock?
    in_stock.zero? || in_stock.negative? || stocks.blank?
  end

  def self.out_of_stock
    all.order(:name).select{ |a| a.out_of_stock?}
  end

  def low_stock?
    if stocks.present?
      if !any_expired?
        in_stock <= stock_alert_count && !out_of_stock?
      else
        false
      end
    else
      false
    end
  end

  def self.low_stock
    low_stock = all.where()
    all.order(:name).select{ |p| p.low_stock? }
  end

  def any_expired?
    if stocks.present?
      stocks.expired.present?
    end
  end

  def any_expired_and_low_stock?
    any_expired? && low_stock?
  end

  def all_expired?
    if stocks.present?
      stocks.count == stocks.select{|s| s.expired?}.count
    else
      false
    end
  end

  def stock_alert
    if out_of_stock?
      "Out of stock"
    elsif low_stock?
      "Low stock"
    elsif low_stock? && !any_expired?
      "Low stock"
    elsif any_expired_and_low_stock? || any_expired?
      "#{stocks.where(deleted: false).expired.sum{|s| s.in_stock }} expired"
    elsif all_expired?
      "All expired"
    end
  end

  def set_status!
    if !out_of_stock?
      self.update(available: true)
    elsif out_of_stock? || all_expired?
      self.update(available: false)
    end
  end

  def zero_alert_count
    if self.stock_alert_count <= 0
      self.errors.add(:stock_alert_count, "can't be zero or negative.")
    end
  end

  private
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end
end
