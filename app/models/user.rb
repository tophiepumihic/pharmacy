class User < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_name, :against => [:full_name, :first_name, :last_name]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  scope :by_total_credit, -> {with_credits.to_a.sort_by(&:total_credit).reverse }
  has_one :address, dependent: :destroy
  has_many :sales, foreign_key: 'employee_id', class_name: 'Order'
  has_many :vouchers, foreign_key: 'employee_id'
  has_many :stock_tansfers, foreign_key: 'employee_id'
  has_many :refunds, foreign_key: 'employee_id'
  has_many :entries, class_name: "Accounting::Entry", foreign_key: 'commercial_document_id'

  accepts_nested_attributes_for :address
  
  enum role:[:cashier, 
            :retail_cashier,
            :wholesale_cashier, 
            :admin, 
            :proprietor, 
            :developer]
  
  has_one_attached :avatar
  validates :first_name, :last_name, :role, presence: true
  
  before_save :set_full_name
  after_save :set_default_image, on: :create
  
  delegate :details, to: :address, prefix: true, allow_nil: true
  
  def fullname
    "#{first_name} #{last_name}"
  end

  private
  def set_full_name
    self.full_name = fullname
  end
  def set_default_image
    if !avatar.attached?
      self.avatar.attach(io: File.open(Rails.root.join('app', 'assets', 'images', 'default.png')), filename: 'default-image.png', content_type: 'image/png')
    end
  end
end
