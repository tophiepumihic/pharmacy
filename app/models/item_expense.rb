class ItemExpense < ApplicationRecord
  belongs_to :stock
  belongs_to :product
  enum expense_type: [:expired, :damaged, :lost]
  BLACKLISTED_TYPES = ['expired']
  validates :quantity, :expense_type, presence: true

  def self.whitelisted_expense_types
    expense_types.keys - BLACKLISTED_TYPES
  end

  def amount
  	quantity * stock.unit_cost
  end

  def remaining_stock_amount
  	remaining_stock_quantity * stock.unit_cost
  end

  def remaining_stock_quantity
  	stock.in_stock - quantity
  end

  def update_stock_quantity
  	stock.in_stock + quantity
  end

  def update_stock_amount
  	update_stock_quantity * stock.unit_cost
  end

  def create_expense_from_stock!
  	@spoilage_breakage_and_loses = Accounting::Account.find_by(name: "Spoilage, Breakage and Losses")
		@merchandise_inventory = Accounting::Account.find_by(name: "Merchandise Inventory")
  	if stock.expired?
	    stock.create_expense_from_expired_stock!
	  else
	  	if quantity == stock.in_stock
		    Accounting::Entry.create!(stock_id: self.stock.id, commercial_document_id: self.id, 
		      commercial_document_type: self.class, date: Time.zone.now, description: "#{self.expense_type.titleize} stock of #{self.quantity} #{self.stock.product.unit} #{self.stock.product.name_and_description}", 
		      debit_amounts_attributes: [amount: self.amount, account: @spoilage_breakage_and_loses], 
		      credit_amounts_attributes:[amount: self.amount, account: @merchandise_inventory], 
		      employee_id: self.employee_id)

		    self.stock.update(deleted: true)
		  else
		    Accounting::Entry.create!(stock_id: self.stock.id, commercial_document_id: self.id, 
		      commercial_document_type: self.class, date: Time.zone.now, description: "#{self.expense_type.titleize} stock of #{self.quantity} #{self.stock.product.unit} #{self.stock.product.name_and_description}", 
		      debit_amounts_attributes: [amount: self.amount, account: @spoilage_breakage_and_loses], 
		      credit_amounts_attributes:[amount: self.amount, account: @merchandise_inventory], 
		      employee_id: self.employee_id)
		  end
	  end
  end

  def remove_stock_from_expense!
  	if quantity == stock.in_stock || stock.expired?
	    @entry = Accounting::Entry.where(commercial_document_id: self.id).where(commercial_document_type: self.class.name).last
	    if @entry.present?
	      @entry.destroy
	    else
	      false
	    end
	    stock.update(deleted: false)
	  else
	  	@entry = Accounting::Entry.where(commercial_document_id: self.id).where(commercial_document_type: self.class.name).last
	    if @entry.present?
	      @entry.destroy
	    else
	      false
	    end
	  end
  end
end
