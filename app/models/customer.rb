class Customer < ApplicationRecord
  include PgSearch
  pg_search_scope :search_by_name, :against => [:full_name]

  enum member_type:[:regular, :irregular]

  has_many :orders
  has_many :line_items, through: :orders
  has_many :entries, class_name: "Accounting::Entry", foreign_key: 'commercial_document_id'
  has_many :debit_amounts, through: :entries
  has_many :credit_amounts, through: :entries
  has_one :address, dependent: :destroy

  has_one_attached :avatar

  accepts_nested_attributes_for :address
  validates :first_name, :last_name, presence: true
  before_save :set_full_name

  delegate :details, to: :address, prefix: true, allow_nil: true

  def self.types
    %w(Member Guest Organization)
  end

  def self.import(file)
    spreadsheet = Roo::Spreadsheet.open(file.path)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      member = find_by(id: row["id"]) || new
      member.attributes = row.to_hash
      member.save!
    end
  end

  def to_s
    full_name
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def reversed_name
    "#{last_name}, #{first_name}"
  end

  def credit_items
    line_items.select { |l| l.order.pay_type == "credit" }
  end

  def self.with_credits
    all.select{|a| a.has_credit == true }
  end

  def self.with_payments
    all.select{|a| a.entries.credit_payment.any? }
  end

  def self.with_cash_advance_balance
    all.select {|a| a.total_cash_advance_balance > 0.0}
  end

  def with_credits?
    total_remaining_balance > 0.0
  end

  def self.total_payment
    all.map{|a| a.total_payment }.sum
  end

  def self.total_credit
    all.map{|a| a.total_credit }.sum
  end

  def total_credit
    Accounting::Account.find_by_name('Accounts Receivables Trade - Current').debit_entries.where(commercial_document_id: self.id).distinct.pluck(:amount).sum
    # Accounting::Account.find_by_name("Accounts Receivables Trade - Current").entries.where(:commercial_document_id => self.id).map{|a| a.credit_amounts.pluck(:amount).sum}.sum
  end

  def total_payment
    Accounting::Account.find_by_name('Accounts Receivables Trade - Current').credit_entries.where(commercial_document_id: self.id).distinct.pluck(:amount).sum 
  end

  def payment_entries
    Accounting::Account.find_by_name('Accounts Receivables Trade - Current').credit_entries.where(commercial_document_id: self.id).order(:date).distinct
  end

  def cash_advance_entries
    Accounting::Account.find_by_name('Advances to Officers, Employees and Members').debit_entries.where(commercial_document_id: self.id).order(date: :desc).distinct
  end

  def total_cash_advance
    Accounting::Account.find_by_name('Advances to Officers, Employees and Members').debit_entries.where(commercial_document_id: self.id).distinct.pluck(:amount).sum
  end

  def cash_advance_payment_entries
    Accounting::Account.find_by_name('Advances to Officers, Employees and Members').credit_entries.where(commercial_document_id: self.id).order(date: :desc).distinct
  end

  def total_cash_advance_payment
    Accounting::Account.find_by_name('Advances to Officers, Employees and Members').credit_entries.where(commercial_document_id: self.id).distinct.pluck(:amount).sum
  end

  def total_cash_advance_balance
    total_cash_advance - total_cash_advance_payment
  end

  def last_total_balance
    total_remaining_balance + last_payment_amount
  end

  def last_payment_amount
    if payment_entries.present?
      Accounting::Account.find_by_name('Accounts Receivables Trade - Current').credit_entries.where(commercial_document_id: self.id).last.credit_amounts.sum(:amount)
    end
  end
  
  def total_remaining_balance
    total_credit_transactions - total_payment
  end

  def total_cash_transactions
    orders.cash.map{|a| a.total_amount_less_discount}.sum
  end

  def total_credit_transactions
    orders.credit.map{|a| a.total_amount_less_discount}.sum
  end

  def annual_cash_transactions
    orders.where(date: Time.now.beginning_of_year..Time.now.end_of_year).sum {|o| o.line_items.where(paid: true).sum(:total_price)}
  end

  def total_discount 
    orders.map{|a| a.discount.amount}.sum
  end

  def self.total_remaining_balance
    all.map{|a| a.total_remaining_balance}.sum
  end

  def self.with_cash_transactions
    all.select{|m| m.annual_cash_transactions != 0}
  end

  def last_cash_advance_created_at
    if cash_advance_entries.present?
      Accounting::Account.find_by_name('Advances to Officers, Employees and Members').debit_entries.where(commercial_document_id: self.id).last.date
    end
  end

  def last_cash_advance_payment_created_at
    if cash_advance_entries.present?
      Accounting::Account.find_by_name('Advances to Officers, Employees and Members').credit_entries.where(commercial_document_id: self.id).last.date
    end
  end

  def first_credit_created_at
    if line_items.credit.present?
      orders.credit.first.date
    end
  end

  def last_credit_created_at
    if line_items.credit.present?
      orders.credit.last.date
    end
  end

  def last_cash_order_created_at
    if line_items.cash.present?
      orders.cash.last.date
    end
  end

  def last_interest_created_at
    if interest_entries.present?
      Accounting::Account.find_by_name('Interest Income from Credit Sales').credit_entries.where(commercial_document_id: self.id).last.date
    end
  end

  def last_payment_created_at
    if payment_entries.present?
      Accounting::Account.find_by_name('Accounts Receivables Trade - Current').credit_entries.where(commercial_document_id: self.id).last.date
    end
  end

  def set_has_credit_to_false!
    if total_remaining_balance == 0.0
      self.update(has_credit: false)
    end
  end

  def unpaid_items
    line_items.where(paid: false).all
  end

  private
  def set_full_name
    self.full_name = fullname
  end

end
