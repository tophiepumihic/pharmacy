module Accounting
  class EntryPolicy < ApplicationPolicy
    def initialize(employee, entry)
      @employee = employee
      @entry = entry
    end
    def index?
      @employee.proprietor? || @employee.developer? || @employee.admin?
    end
    def new?
      create?
    end
    def create?
      @employee.proprietor? || @employee.developer? || @employee.admin?
    end
  end
end
