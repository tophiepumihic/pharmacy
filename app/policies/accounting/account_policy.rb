module Accounting
  class AccountPolicy < ApplicationPolicy
    def initialize(employee, account)
      @employee = employee
      @account = account
    end
    def index?
    	@employee.proprietor? || @employee.developer? || @employee.admin?
    end
  end
end
