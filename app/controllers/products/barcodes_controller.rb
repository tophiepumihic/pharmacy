module Products
  class BarcodesController < ApplicationController
    def new
      @product = Product.find(params[:product_id])
      @barcode = @product.barcodes.build
    end

    def create
      @product = Product.find(params[:product_id])
      @barcode = @product.barcodes.create(barcode_params)
    end

    def edit
      @product = Product.find(params[:product_id])
      @barcode = Barcode.find(params[:id])
    end

    def update
      @product = Product.find(params[:product_id])
      @barcode = @product.barcodes.find(params[:id])
      @barcode.update(barcode_params)
    end

    def destroy
      @product = Product.find(params[:product_id]) if params[:product_id].present?
      @barcode = Barcode.find(params[:id])
      @barcode.destroy
      redirect_to barcode_product_path(@product), alert: 'Barcode deleted successfully.'
    end
    
    private
    def barcode_params
      params.require(:barcode).permit(:code)
    end
  end
end
