module Products
  class StocksController < ApplicationController
    def new
      @product = Product.find(params[:product_id])
      @available = @product.stocks.where(deleted: false).order(date: :desc)
      @stocks = Kaminari.paginate_array(@available).page(params[:page]).per(30)
      @stock = @product.stocks.build
    end

    def create
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.create(stock_params)
      @stock.employee = current_user
      if @stock.save
        ActiveRecord::Base.transaction do
          @stock.create_entry
          @stock.set_status!
          @stock.product.set_status!
        end
        redirect_to stock_histories_product_path(@product), notice: "New stock saved successfully."
      else
        render :new
      end
    end

    def edit
      @product = Product.find(params[:product_id])
      @stock = Stock.find(params[:id])
    end

    def update
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:id])
      if @stock.update(stock_params)
        ActiveRecord::Base.transaction do
          @stock.update_entry
          @stock.set_status!
          @stock.product.set_status!
        end
        redirect_to stock_histories_product_path(@product), notice: "Stock updated successfully."
      end
    end

    def show
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:id])
    end

    def delete
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:stock_id])
      if @stock.in_stock.zero?
        @stock.delete!
        redirect_to stock_histories_product_path(@product), alert: 'Stocks deleted successfully.'
      else
        redirect_to stock_histories_product_path(@product), alert: "Unable to delete stock, #{@stock.in_stock} #{@product.unit} still remains and not expired."
      end
    end

    def add_to_expenses
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:stock_id])
      if @stock.in_stock != 0 && @stock.expired?
        @stock.create_expense_from_expired_stock!
        redirect_to stock_histories_product_path(@product), alert: "#{@stock.in_stock} #{@product.unit} of #{@product.name_and_description} with total amount of P#{@stock.in_stock_amount} has been added to expenses."
      else
        redirect_to stock_histories_product_path(@product), alert: "Cannot add to expenses. Not out of stock or expired."
      end
    end

    def remove_from_expenses
      @product = Product.find(params[:product_id])
      @stock = @product.stocks.find(params[:stock_id])
      @stock.remove_expense_from_expired_stock!
      redirect_to stock_histories_product_path(@product), alert: "#{@stock.in_stock} #{@product.unit} of #{@product.name_and_description} with total amount of P#{@stock.in_stock_amount} has been removed from expenses."
    end

    def destroy
      @product = Product.find(params[:product_id]) if params[:product_id].present?
      @stock = Stock.find(params[:stock_id])
      if @stock.line_items.blank?
        @stock.destroy
        redirect_to stock_histories_product_path(@product), alert: 'Stock deleted successfully.'
      else
        @stock.delete!
        redirect_to stock_histories_product_path(@stock), alert: "Stock deleted successfully."
      end
    end
    
    private
    def stock_params
      params.require(:stock).permit(:lot_number, :payment_type, :supplier_id, :reference_number, :quantity, :date, :total_cost, :expiry_date, :unit_cost)
    end
  end
end
