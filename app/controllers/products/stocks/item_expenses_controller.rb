module Products
	module Stocks
		class ItemExpensesController < ApplicationController

			def new
				@stock = Stock.find(params[:stock_id])
				@product = @stock.product
				@item_expense = @stock.item_expenses.build
			end

			def create
				@stock = Stock.find(params[:stock_id])
				@product = @stock.product
				@item_expense = @stock.item_expenses.create(item_expense_params)
				@item_expense.create_expense_from_stock!
			end

			def show
				@item_expense = ItemExpense.find(params[:id])
				@stock = @item_expense.stock
				@product = @item_expense.product
			end

			def remove_from_expenses
				@item_expense = ItemExpense.find(params[:item_expense_id])
	      @stock = @item_expense.stock
				@product = Product.find(params[:product_id])
	      @item_expense.remove_stock_from_expense!
	      @item_expense.destroy
	      redirect_to stock_histories_product_path(@product), alert: "#{@item_expense.quantity} #{@product.unit} of #{@product.name_and_description} with total amount of P#{@item_expense.amount} has been removed from expenses."
	    end

			private
				def item_expense_params
					params.require(:item_expense).permit(:quantity, :expense_type, :employee_id, :product_id)
				end
		end
	end
end