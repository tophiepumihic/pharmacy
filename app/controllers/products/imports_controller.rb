module Products
	class ImportsController < ApplicationController

		def create
	      @import = ProductImport.new(import_params).parse_records!
	      redirect_to products_url, notice: 'Products Imported'
	  end

	  private
	  def import_params
	  	params.require(:product_import).permit(:spreadsheet_file, :employee_id)
	  end
	end
end