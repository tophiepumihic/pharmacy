class ProductsController < ApplicationController

  autocomplete :product, :name_and_description, full: true

  def index
    if params[:name_and_description].present?
      @products = Product.search_by_name(params[:name_and_description]).page(params[:page]).per(30)
    else
      @items = Product.order(:name).all
      @products = Kaminari.paginate_array(@items).page(params[:page]).per(30)
      respond_to do |format|
        format.html
        format.pdf do
          pdf = ProductsPdf.new(@items, view_context)
                send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Products Report.pdf"
        end
      end
    end
    authorize Product
  end

  def download
    @products = Product.order(:name)
    respond_to do |format|
      format.xlsx { render xlsx: "download", disposition: 'inline', filename: "products-#{Time.zone.now.strftime('%B %e, %Y')}" }
    end
  end

  def export
    @products = Product.order(:name)
    respond_to do |format|
      format.xlsx { render xlsx: "export", disposition: 'inline', filename: "ProductsExport-#{Time.zone.now.strftime('%B %e, %Y')}" }
    end
  end

  def new
    @product = Product.new
    @product.stocks.build
    authorize @product
  end

  def create
    @products = Product.all
    @product = Product.create(product_params)
    @product.set_status!
  end

  def barcode
    @product = Product.find(params[:id])
    @codes = @product.barcodes.all
    @barcodes = Kaminari.paginate_array(@codes).page(params[:page]).per(50)
  end

  def stock_histories
    @product = Product.find(params[:id])
    @available = @product.stocks.where(deleted: false).order(date: :desc)
    @stocks = Kaminari.paginate_array(@available).page(params[:page]).per(50)
  end

  def cash_sales
    @product = Product.find(params[:id])
    @sales = @product.line_items.cash
    @results = Kaminari.paginate_array(@sales).page(params[:page]).per(50)
  end

  def credit_sales
    @product = Product.find(params[:id])
    @sales = @product.line_items.credit
    @results = Kaminari.paginate_array(@sales).page(params[:page]).per(50)
  end

  def deleted
    @product = Product.find(params[:id])
    @stocks = @product.item_expenses.order(created_at: :desc).all + @product.stocks.deleted
    @deleted = Kaminari.paginate_array(@stocks).page(params[:page]).per(50)
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    @product.update(product_params)
    @product.set_status!
  end

  def destroy
    @product = Product.find(params[:id])
    if @product.orders.blank?
      @product.destroy
      redirect_to products_path, notice: 'Product deleted.'
    else
      redirect_to stock_histories_product_path(@product), notice: 'Product has orders, unable to delete.'
    end
  end

  def reports
    @product = Product.find(params[:id])
    @sales = @product.line_items.credit
    @results = Kaminari.paginate_array(@sales).page(params[:page]).per(50)
  end
  
  def scope_to_date_cash_sales
    @product = Product.find(params[:id])
    @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.yesterday.end_of_day
    @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
    @line_items = @product.line_items.cash.created_between({from_date: @from_date, to_date: @to_date})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Products::SalesReportPdf.new(@product, @line_items, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Sales Report.pdf"
      end
    end
  end
  def scope_to_date_sales
    @product = Product.find(params[:id])
    @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.zone.now
    @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.zone.now
    @line_items = @product.line_items.all.created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Products::SalesReportPdf.new(@product, @line_items, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Sales Report.pdf"
      end
    end
  end

  def scope_to_date_credit_sales
    @product = Product.find(params[:id])
    @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.yesterday.end_of_day
    @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
    @orders = @product.line_items.credit.all.created_between({from_date: @from_date, to_date: @to_date})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = OrdersPdf.new(@orders, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Sales Report.pdf"
      end
    end
  end

  private
  def product_params
    params.require(:product).permit(:name, :description, :unit, :retail_price, :botika_price, :pharmacy_price, :stock_alert_count)
  end
end
