class OrdersController < ApplicationController
  autocomplete :customer, :full_name, full: true

  def index 
    @orders = Order.all.page(params[:page]).per(50)
    if params[:name]
      @orders = Order.text_search(params[:name]).page(params[:page]).per(50)
    end
  end
  
  def new
    @cart = current_cart
    if @cart.line_items.empty?
      if request.referer == store_index_url
        redirect_to store_index_url, notice: "Your cart is empty"
      elsif request.referer == botika_wholesales_url
        redirect_to botika_wholesales_url, notice: "Your cart is empty"
      elsif request.referer == pharmacy_wholesales_url
        redirect_to pharmacy_wholesales_url, notice: "Your cart is empty"
      end
      return
    end
    @order = Order.new
    @order.build_discount
  end

  def create
    @order = Order.new(order_params)
    @order_expense_amount = params[:expense_amount]
    @order.employee = current_user
    @order.add_line_items_from_cart(current_cart)
    @order.set_to_paid!
    @line_items = @order.line_items
            
    respond_to do |format|
      if @order.save
        InvoiceNumber.new.generate_for(@order)
        @order.create_entry
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        @order.delete_stock_if_zero!
        format.html do
          if @order.credit?
            @order.set_customer_has_credit_to_true!
            @order.set_partial_payment!
            redirect_to print_order_url(@order), notice: 'Credit transaction saved successfully.'
          elsif @order.cash?
            redirect_to print_order_url(@order), notice: 'Thank you for your order.'
          end
        end
        format.json { render json: @order, status: :created, location: @order }
      else
        @cart = current_cart
        format.html { render action: "new" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def retail
    @total_sales = Order.retail.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).sum(&:total_amount_less_discount)
    @cash_sales = Order.retail.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).cash.sum(&:total_amount_less_discount)
    @credit_sales = Order.retail.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).credit.sum(&:total_amount_less_discount)
    @total_retail_and_wholesale = Order.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).sum(&:total_amount_less_discount)
    if params[:full_name].present?
      @orders = Order.retail.text_search(params[:full_name]).page(params[:page]).per(50)
    else
      @retail = Order.retail
      @orders = Kaminari.paginate_array(@retail).page(params[:page]).per(50)
    end
  end
  def wholesale
    @total_sales = Order.wholesale.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).sum(&:total_amount_less_discount)
    @cash_sales = Order.wholesale.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).cash.sum(&:total_amount_less_discount)
    @credit_sales = Order.wholesale.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).credit.sum(&:total_amount_less_discount)
    @total_retail_and_wholesale = Order.created_between({from_date: Time.now.beginning_of_day, to_date: Time.now.end_of_day}).sum(&:total_amount_less_discount)
    if params[:full_name].present?
      @orders = Order.wholesale.text_search(params[:full_name]).page(params[:page]).per(50)
    else
      @wholesales = Order.wholesale
      @orders = Kaminari.paginate_array(@wholesales).page(params[:page]).per(50)
    end
  end

  def show
    @order = Order.find(params[:id])
    @line_items = @order.line_items
  end

  def scope_to_date
    @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.zone.now
    @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.zone.now
    @orders = Order.created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = OrdersPdf.new(@orders, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Purchases Report.pdf"
      end
    end
  end

  def monthly_summary_report
    @month_of = params[:month_of].present? ? Time.parse(params[:month_of]) : Time.zone.now
    @stocks = Stock.created_between({from_date: @month_of.beginning_of_month.yesterday.end_of_day, to_date: @month_of.end_of_month.end_of_day})
    @orders = Order.created_between({from_date: @month_of.beginning_of_month.yesterday.end_of_day, to_date: @month_of.end_of_month.end_of_day})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = MonthlySummaryPdf.new(@stocks, @orders, @month_of, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Summary Report.pdf"
      end
    end
  end

  def guest
    @order = Order.new(order_params)
    @order.add_line_items_from_cart(current_cart)
    @order.member = Member.find_by_first_name('Guest')
    @order.save
    redirect_to store_url, notice:
    'Thank you for your order.'
  end

  def print_invoice
    @order = Order.find(params[:id])
    @line_items = @order.line_items
    respond_to do |format|
      format.pdf do
        pdf = InvoicePdf.new(@order, @line_items, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "order_#{@order.invoice_number}.pdf"
        pdf.autoprint
      end
    end
  end

  def print_official_receipt
    @order = Order.find(params[:id])
    @line_items = @order.line_items
    OfficialReceiptNumber.new.generate_for(@order)
    respond_to do |format|
      forma t.pdf do
        pdf = PosReceiptPdf.new(@order, @line_items, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "order_#{@order.invoice_number}.pdf"
        pdf.print
      end
    end
  end

  def print
    @order = Order.find(params[:id])
    @line_items = @order.line_items
    respond_to do |format|
      format.html
      format.pdf do
        pdf = PosReceiptPdf.new(order, order.line_items, view_context)
        pdf.print
      end
    end
  end

  def return
    @order = Order.find(params[:order_id])
    @order.employee = current_user
    if @order.customer.with_credits?
      @order.set_customer_has_credit_to_false!
    end
    # @order.return_line_items_to_stock!
    @order.create_entry_for_return_order!
    @order.update_stock_status!
    @order.destroy
    redirect_to retail_orders_url, alert: 'Order has been returned.'
  end

  def destroy 
    @order = Order.find(params[:id])
    if @order.line_items.empty?
      @order.destroy
      redirect_to retail_orders_url, alert: "Order deleted successfully."
    else
      @order.return_line_items_to_stock!
      redirect_to @order, alert: "Order can not be deleted. Line Items are present."
    end 
  end
  private
  def order_params
    params.require(:order).permit(:name, :order_type, :customer_id, :cash_tendered, :change, :pay_type, :date, :discounted, discount_attributes:[:amount])
  end
end
