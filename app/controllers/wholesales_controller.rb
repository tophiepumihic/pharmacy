class WholesalesController < ApplicationController
  def botika
    if params[:name_and_description].present?
      @products = Product.search_by_name(params[:name_and_description]).pluck(:id)
      if @products.present?
        @stocks = Stock.order(date: :asc).joins(:product).where(products: { id: @products })
      else
        redirect_to botika_wholesales_path, notice: 'Product is either out of stock or expired.'
      end
    else
      @stocks = Stock.available
    end
    @cart = current_cart
    @line_item = LineItem.new
    @order = Order.new
    @order.build_discount
  end

  def pharmacy
    if params[:name_and_description].present?
      @products = Product.search_by_name(params[:name_and_description]).pluck(:id)
      if @products.present?
        @stocks = Stock.order(date: :asc).joins(:product).where(products: { id: @products })
      else
        redirect_to pharmacy_wholesales_path, notice: 'Product is either out of stock or expired.'
      end
    else
      @stocks = Stock.available
    end
    @cart = current_cart
    @line_item = LineItem.new
    @order = Order.new
    @order.build_discount
  end
end
