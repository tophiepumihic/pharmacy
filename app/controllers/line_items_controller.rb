class LineItemsController < ApplicationController
  def create
    @cart = current_cart
    if @cart.line_items.count < 30
      @line_item = @cart.line_items.create(line_item_params)
      @line_item.set_total_price!
      respond_to do |format|
        if @line_item.valid?
          @line_item.save!
          @line_item.stock.product.set_status!
          if @line_item.retail?
            format.html { redirect_to store_index_url, notice: "Added to cart." }
          elsif @line_item.botika?
            format.html { redirect_to botika_wholesales_url, notice: "Added to cart." }
          elsif @line_item.pharmacy?
            format.html { redirect_to pharmacy_wholesales_url, notice: "Added to cart." }
          end
          format.js { @current_item = @line_item }
        else
          format.html { redirect_to store_index_url, notice: "Quantity must be lesser or equal to stock." }
        end
      end
    else
      redirect_to store_index_url, notice: "Items are only limited to 30 counts per reciept, please checkout and try adding items again."
    end
  end

  def return
    @order = Order.find(params[:order_id])
    @line_item = LineItem.find(params[:line_item_id])
    @line_item.employee = current_user
    @line_item.return_quantity_to_stock!
    @line_item.update_stock_status!
    @line_item.create_entry_for_sales_return!
    @line_item.destroy
    redirect_to retail_orders_path, alert: 'Item has been returned.'
  end

  def destroy
    @line_item = LineItem.find(params[:id])
    @line_item.destroy
    redirect_to store_index_url, alert: "Item has been removed."
  end

  private
  def line_item_params
    params.require(:line_item).permit(:program_id, :pricing_type, :customer_id, :stock_id, :quantity, :unit_price, :total_price)
  end
end
