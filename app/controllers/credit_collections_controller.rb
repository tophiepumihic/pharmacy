class CreditCollectionsController < ApplicationController
  def index
    if params[:full_name].present?
      @members = Customer.search_by_name(params[:full_name]).page(params[:page]).per(30)
    else
    	@payments = Member.with_payments
    	@members = Kaminari.paginate_array(@payments).page(params[:page]).per(40)
      #@members = Member.select{|m| m.annual_total_cash_transactions != 0}
      respond_to do |format|
        format.html
        format.pdf do
          pdf = CreditCollectionsPdf.new(@members, view_context)
                send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "AR Collections Report.pdf"
        end
      end
	  end
  end

  def scope_to_date
    @from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now
    @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now
    @entries = Accounting::Entry.credit_payment.created_between({from_date: @from_date, to_date: @to_date.end_of_day}).order(date: :desc)
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Accounting::AccountsReceivablesCollectionsPdf.new(@entries, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Entries Report.pdf"
      end
    end
  end
end