module Accounting 
  class DisbursementsController < ApplicationController 
    def new
      # @entry = DisbursementForm.new
      # first_entry = Accounting::Entry.order('date ASC').first
      # @from_date = first_entry ? first_entry.date: Time.zone.now
      @entry = Accounting::Entry.new
      authorize @entry
      @entry.debit_amounts.build
      @entry.credit_amounts.build
    end

    def create
      # @entry = DisbursementForm.new(entry_params)
      # if @entry.save
      #   redirect_to accounting_entries_path, notice: 'Entry has been saved successfully.'
      # else
      #   render :new
      # end
      @entry = Accounting::Entry.create(entry_params)
      @entry.recorder = current_user
      if @entry.save
        redirect_to accounting_entries_path, notice: 'Entry has been saved successfully.'
      else
        render :new
      end
      authorize @entry
    end

    private
    def entry_params
      # params.require(:disbursement_form).permit(
      #   :commercial_document_id, 
      #   :commercial_document_type, 
      #   :date, :description, 
      #   :reference_number, 
      #   :expense_account_id,
      #   :amount)
      params.require(:accounting_entry).permit(
        :commercial_document_id, 
        :commercial_document_type, 
        :date, 
        :description, 
        :reference_number, 
        debit_amounts_attributes:[:amount, :account_id], 
        credit_amounts_attributes:[:amount, :account_id])
    end
  end 
end