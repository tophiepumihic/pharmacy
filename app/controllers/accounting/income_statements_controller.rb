module Accounting
  class IncomeStatementsController < ApplicationController
    def index
      @from_date = params[:from_date] ? Date.parse(params[:from_date]) : Time.zone.now.at_beginning_of_month
      @to_date = params[:to_date] ? Date.parse(params[:to_date]) : Time.zone.now
      @revenues = Accounting::Revenue.active_with_balance
      @expenses = Accounting::Expense.active_with_balance

      respond_to do |format|
        format.html # index.html.erb
        format.pdf do
          pdf = Accounting::IncomeStatementPdf.new(@revenues, @expenses,  @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Income Statement.pdf"
        end
      end
    end

    def scope_to_date
      @from_date = params[:from_date] ? Date.parse(params[:from_date]) : Time.zone.now
      @to_date = params[:to_date] ? Date.parse(params[:to_date]) : Time.zone.now
      @scope = params[:scope]
      @spoilage = Accounting::Account.find_by_name("Spoilage, Breakage and Losses")
      @revenues = Accounting::Revenue.all
      @expenses = Accounting::Expense.where.not(id: @spoilage.id).all

      respond_to do |format|
        format.html # index.html.erb
        format.pdf do
          pdf = Accounting::IncomeStatementPdf.new(@scope, @revenues, @expenses,  @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Income Statement.pdf"
        end
      end
    end
  end
end