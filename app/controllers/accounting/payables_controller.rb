module Accounting
  class PayablesController < ApplicationController

  	def new
      @entry = Accounting::Entry.new
      authorize @entry
      @entry.debit_amounts.build
      @entry.credit_amounts.build
    end

    def create
      @entries = Accounting::Entry.all
      @entry = Accounting::Entry.create(entry_params)
      @entry.recorder = current_user
      if @entry.save
        redirect_to accounting_entries_path, notice: 'Entry for sales has been saved successfully.'
      else
        render :new
      end
      authorize @entry
    end

    private
    def entry_params
      params.require(:accounting_entry).permit(:date, :entry_type, :commercial_document_id, :commercial_document_type, :description, :reference_number, debit_amounts_attributes:[:amount, :account_id], credit_amounts_attributes:[:amount, :account_id])
    end
  end
end