module Accounting
  class BalanceSheetsController < ApplicationController
    def index
      first_entry = Accounting::Entry.order('date ASC').first
      @from_date = first_entry ? first_entry.date: Date.today
      @to_date = params[:date] ? Date.parse(params[:date]) : Date.today
      @assets = Accounting::Asset.active_with_balance
      @liabilities = Accounting::Liability.active_with_balance
      @equity = Accounting::Equity.active_with_balance
      @revenues = Accounting::Revenue.active_with_balance
      @expenses = Accounting::Expense.active_with_balance

      respond_to do |format|
        format.html # index.html.erb
      end
    end
    def scope_to_date
      first_entry = Accounting::Entry.order('date ASC').first
      @from_date = first_entry ? first_entry.date: Time.zone.now
      #@from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now
      @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now
      @scope = params[:scope]
      @assets = Accounting::Asset.all
      @liabilities = Accounting::Liability.all
      @expenses = Accounting::Expense.all
      @revenues = Accounting::Revenue.all

      respond_to do |format|
        format.html # index.html.erb
        format.pdf do
          pdf = Accounting::BalanceSheetPdf.new(@scope, @revenues, @liabilities, @assets, @expenses, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Balance Sheet.pdf"
        end
      end
    end
  end
end