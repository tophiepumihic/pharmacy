class OneMonthExpiryProductsController < ApplicationController
  def index
    @stocks = Stock.one_month_to_expire
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Stocks::OneMonthBeforePdf.new(@stocks, view_context)
              send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Expired Stock Report.pdf"
      end
    end
  end
end
