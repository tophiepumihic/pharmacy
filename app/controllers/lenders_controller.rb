class LendersController < ApplicationController

  def new
    @lender = Lender.new
    @lender.build_address
  end

  def create
    @lender = Lender.create(member_params)
  end

  def edit
    @lender = Lender.find(params[:id])
    @address = @lender.address
  end

  def update
    @lender = Lender.find(params[:id])
    @lender.update_attributes(member_params)
  end

  private
  def member_params
    params.require(:lender).permit(:last_name, :first_name, :middle_name, :member_type, :mobile, :profile_photo, address_attributes:[:id, :house_number, :street, :barangay, :municipality, :province, :id ])
  end
end
