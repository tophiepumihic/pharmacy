class CashAdvancesController < ApplicationController
  def index
    if params[:full_name].present?
      @members = Customer.search_by_name(params[:full_name]).page(params[:page]).per(30)
    else
    	@payments = Customer.with_cash_advance_balance
    	@members = Kaminari.paginate_array(@payments).page(params[:page]).per(40)
      #@members = Member.select{|m| m.annual_total_cash_transactions != 0}
      respond_to do |format|
        format.html
        format.pdf do
          pdf = CashAdvancesPdf.new(@members, view_context)
                send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "AR Collections Report.pdf"
        end
      end
	  end
  end
end