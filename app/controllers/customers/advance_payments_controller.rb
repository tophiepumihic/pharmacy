module Customers
  class AdvancePaymentsController < ApplicationController
    def scope_to_date
      @member = Customer.find(params[:customer_id])
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      @advance_payments = @member.cash_advance_payment_entries.order(:date).created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
      respond_to do |format|
        format.html
        format.pdf do
          pdf = Customers::AdvancePaymentsPdf.new(@member, @advance_payments, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Account Details.pdf"
          pdf.print
        end
      end
    end

    def new
      @member = Customer.find(params[:customer_id])
      @credit_orders = @member.orders.credit.order(date: :desc).all.page(params[:page]).per(10)
      @entry = Accounting::Entry.new
      @entry.debit_amounts.build
      @entry.credit_amounts.build
    end

    def create
      @member = Customer.find(params[:customer_id])
      @credit_orders = @member.orders.credit.order(date: :desc).all.page(params[:page]).per(10)
      @entry = Accounting::Entry.new(entry_params)
      if @entry.save
        @member.set_has_credit_to_false!
        redirect_to advance_payments_customer_path(@member), notice: "Payment saved successfully."
      else
        render :new
      end
      # authorize @entry
    end

    def show
      @entry = Accounting::Entry.find(params[:id])
    end

    private
    def entry_params
      params.require(:accounting_entry).permit(
        :date, 
        :description, 
        :reference_number, 
        :commercial_document_id, 
        :commercial_document_type, 
        :employee_id, 
        debit_amounts_attributes:[:amount, :account_id], 
        credit_amounts_attributes:[:amount, :account_id])
    end
  end
end
