module Customers
  class AdvancesController < ApplicationController
    def scope_to_date
      @member = Customer.find(params[:customer_id])
      @advances = @member.cash_advance_entries.order(:date).all
      @from_date = params[:from_date] ? DateTime.parse(params[:from_date]) : Time.now.beginning_of_day
      @to_date = params[:to_date] ? DateTime.parse(params[:to_date]) : Time.now.end_of_day
      respond_to do |format|
        format.html
        format.pdf do
          pdf = Customers::AdvancesPdf.new(@member, @advances, @from_date, @to_date, view_context)
            send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Cash Advances.pdf"
          pdf.print
        end
      end
    end

    def new
      @member = Customer.find(params[:customer_id])
      @entry = Accounting::Entry.new
      authorize @entry
      @entry.debit_amounts.build
      @entry.credit_amounts.build
    end

    def create
      @member = Customer.find(params[:customer_id])
      @entry = Accounting::Entry.create(entry_params)
      @entry.recorder = current_user
      if @entry.save
        redirect_to advances_customer_path(@member), notice: 'Entry has been saved successfully.'
      else
        render :new
      end
      authorize @entry
    end

    private
    def entry_params
      params.require(:accounting_entry).permit(
        :commercial_document_id,
        :commercial_document_type,
        :entry_type,
        :date, 
        :description, 
        :reference_number, 
        debit_amounts_attributes:[:amount, :account_id], 
        credit_amounts_attributes:[:amount, :account_id])
    end
  end
end
