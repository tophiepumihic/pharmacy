class StocksController < ApplicationController

	def scope_to_date
    @from_date = params[:from_date] ? Time.parse(params[:from_date]) : Time.zone.now
    @to_date = params[:to_date] ? Time.parse(params[:to_date]) : Time.zone.now
    @stocks = Stock.created_between({from_date: @from_date.yesterday.end_of_day, to_date: @to_date.end_of_day})
    respond_to do |format|
      format.html
      format.pdf do
        pdf = StocksPdf.new(@stocks, @from_date, @to_date, view_context)
          send_data pdf.render, type: "application/pdf", disposition: 'inline', file_name: "Purchases Report.pdf"
      end
    end
  end
end