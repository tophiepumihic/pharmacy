class DisbursementForm
	include ActiveModel::Model 
	attr_accessor :expense_account_id, :amount, :date, :description, :employee_id, :reference_number, :commercial_document_id, :commercial_document_type
  validates :amount, numericality: true
  validates :expense_account_id, :amount, presence: true
  
  def save 
    ActiveRecord::Base.transaction do
      save_entry
    end
  end

  private
    def save_entry
      accrued_expenses = Accounting::Account.find_by(name: "Accrued Expenses")
      cash_on_hand = Accounting::Account.find_by(name: "Cash on Hand")
      expense_account = Accounting::Account.find(expense_account_id)

    	Accounting::Entry.disbursement.create!(date: date, reference_number: reference_number, 
    		commercial_document_type: commercial_document_type, employee_id: employee_id,
    		commercial_document_id: commercial_document_id, description: description,
    		debit_amounts_attributes: [
          {amount: amount, account: accrued_expenses},
    			{amount: amount, account: expense_account}
        ],
    		credit_amounts_attributes: [ 
    			{amount: amount, account: expense_account},
    			{amount: amount, account: cash_on_hand}
        ])
    end
end