class ProductImport
	require 'roo'
	include ActiveModel::Model

	attr_accessor :spreadsheet_file, :employee_id

  def parse_records!
    product_spreadsheet = Roo::Spreadsheet.open(spreadsheet_file.path)
    header = product_spreadsheet.row(1)
    (2..product_spreadsheet.last_row).each do |i|
      row = Hash[[header, product_spreadsheet.row(i)].transpose]
      ActiveRecord::Base.transaction do 
        find_or_create_product(row)
      end
    end
  end

  def find_or_create_product(row)
    product = Product.where(name: row['Item Name']).first_or_create! do |p|
      p.unit              = row["Unit"].present? ? row["Unit"] : "PC"
      p.retail_price      = row['Retail Price']
      p.botika_price      = row["Botika Price"].present? ? row['Botika Price'] : 0
      p.pharmacy_price    = row["Pharmacy Price"].present? ? row['Pharmacy Price'] : 0
      p.stock_alert_count = 1
    end
    create_stock(row, product) unless row["Quantity"].blank? || row["Quantity"].zero?
  end

  def create_stock(row, product)
  	product.stocks.create!(
  		quantity:    row["Quantity"],
  		unit_cost:   unit_cost(row["Unit Cost"], product),
  		total_cost:  compute_total_cost(row),
      date:        parse_date(row["Date Purchased"]),
      expiry_date: parse_expiry_date(row["Expiry Date"]),
      lot_number:  row["Lot Number"],
      employee:    find_employee,
  		supplier:    find_supplier
  	)
  end

  def parse_date(date)
    if date.present?
      Date.parse(date.to_s)
    else
      Date.today
    end
  end

  def parse_expiry_date(date)
    if date.present?
      Date.parse(date.to_s)
    else
      nil
    end
  end

  def unit_cost(cost, product)
    if cost.present?
      cost
    else
      product.retail_price * 0.85
    end
  end

  def compute_total_cost(row)
  	(row["Quantity"].nil? ? 0 : row["Quantity"]) * row["Unit Cost"]
  end

  def find_supplier
  	Supplier.where(business_name: "Old Stock").first_or_create! do |s|
      s.owner = Business.last.name
    end
  end

  def find_employee
    Employee.find(employee_id)
  end
end